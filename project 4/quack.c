#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#include "quack.h"

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int LineNumber;

void yyerror(const char *Text);

node_ast *RootNode;

char Obj[] = "Obj";

static b32 TypeBubbling;

node_ast *
NewListNode(list_type ListType)
{
    node_ast *Result;
    
    node_ast *NodeAst = new node_ast;
    NodeAst->List.List = new list<node_ast *>();
    NodeAst->ListType = ListType;
    NodeAst->AstType = AstType_non_terminal_list;
    
    Result = NodeAst;
#if 0
    printf("Creating List Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal_list, Result->AstType);
    printf("ListType: %d ResultType: %d\n", ListType, Result->ListType);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewNonTerminalNode(s32 LineNumber, non_terminal_type NonTerminalType, s32 ChildCount, ...)
{
    node_ast *Result = new node_ast;
    if(Result)
    {
        Result->LineNumber = LineNumber;
        Result->AstType = AstType_non_terminal;
        Result->ListType = ListType_empty;
        Result->NonTerminal.Type = NonTerminalType;
        Result->NonTerminal.ChildCount = ChildCount;
        
        list<node_ast *> *List;
        
        va_list VaList;
        va_start(VaList, ChildCount);
        for(s32 Index = 0;
            Index < ChildCount;
            ++Index)
        {
            Result->NonTerminal.Children[Index] = va_arg(VaList, node_ast *);
        }
        
        va_end(VaList);
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
#if 0
    printf("Creating Non Terminal Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal, Result->AstType);
    printf("NonTerminalType: %d ResultType: %d\n", NonTerminalType, Result->NonTerminal.Type);
    printf("ListType: %d ResultType: %d\n", ListType_empty, Result->ListType);
    printf("ChildCount: %d ResultCount: %d\n", ChildCount, Result->NonTerminal.ChildCount);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewIdentNode(s32 LineNumber, char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->LineNumber = LineNumber;
        Result->AstType = AstType_ident;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewIntLitNode(s32 LineNumber, char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->LineNumber = LineNumber;
        Result->AstType = AstType_int_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewStringLitNode(s32 LineNumber, char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->LineNumber = LineNumber;
        Result->AstType = AstType_string_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

b32
CompareStrings(char *String1, char *String2)
{
    b32 Result = true;
#if 1
    char *A = String1;
    char *B = String2;
    
    while(*A != '\0' && *B != '\0')
    {
        if(*A++ != *B++)
        {
            Result = false;
            break;
        }
    }
    
    if(*A != *B)
    {
        Result = false;
    }
#endif
    return(Result);
}

node_ast *
FindClassNode(char *Name, list<node_ast *> *ClassList)
{
    node_ast *Result = 0;
    
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *FirstChild = (*Iterator)->NonTerminal.Children[0];
            if(FirstChild)
            {
                char *ClassName = FirstChild->NonTerminal.Children[0]->Ident.Text;
                
                if(CompareStrings(ClassName, Name))
                {
                    Result = FirstChild;
                    break;
                }
            }
        }
    }
    
    return(Result);
}

void
ClearVisits(list<node_ast *> *ClassList)
{
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *Node = (*Iterator)->NonTerminal.Children[0];
            Node->Visited = false;
        }
    }
}

void
WalkAst(node_ast *Node,  list<node_ast *> *ClassList, b32 *ConstructorsWellFormed)
{
    if(Node)
    {
#if 0
        printf("Walking Node\n");
        printf("AstType: %d \n", Node->AstType);
        printf("ListType: %d\n", Node->ListType);
        printf("\n--------------------\n");
#endif
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                if(Node->NonTerminal.Type == NonTerminalType_constructor)
                {
                    node_ast *ConstructorIdentNode = Node->NonTerminal.Children[0];
                    char *ConstructorName = ConstructorIdentNode->Ident.Text;
                    if(!FindClassNode(ConstructorName, ClassList))
                    {
                        char Obj[] = "Obj";
                        if(!CompareStrings(ConstructorName, Obj))
                        {
                            *ConstructorsWellFormed = false;
                        }
                    }
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        WalkAst(Node->NonTerminal.Children[Index], ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::const_iterator Iterator;
                for(Iterator = Node->List.List->begin();
                    Iterator != Node->List.List->end();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        WalkAst(*Iterator, ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
        }
    }
}

quack_scope *
AddScope(quack_scope_list *List, char *Name)
{
    quack_scope *Result = 0;
    
    if(List->ListSize < 1024)
    {
        quack_scope *Scope = new quack_scope;
        Scope->ScopeName = Name;
        Scope->NameCount = 0;
        List->List[List->ListSize++] = Scope;
        Result = Scope;
    }
    else
    {
        printf("quack_scope_list size got too big!\n");
    }
    
    return(Result);
}

quack_scope *
FindScope(quack_scope_list *List, char *Name)
{
    quack_scope *Result = 0;
    
    for(s32 Index = 0;
        Index < List->ListSize;
        ++Index)
    {
        if(CompareStrings(List->List[Index]->ScopeName, Name))
        {
            Result = List->List[Index];
        }
    }
    
    return(Result);
}

quack_scope *
FindScopeWithClassScope(quack_scope_list *List, char *Name, char *ClassScopeName)
{
    quack_scope *Result = 0;
    
    for(s32 Index = 0;
        Index < List->ListSize;
        ++Index)
    {
        if(CompareStrings(List->List[Index]->ScopeName, Name))
        {
            quack_scope *Scope = List->List[Index];
            if(Scope && Scope->ClassScope)
            {
#if 1
                if(CompareStrings(Scope->ClassScope->ScopeName, ClassScopeName))
                {
                    Result = Scope;
                }
#endif
            }
        }
    }
    
    return(Result);
}

s32
ScopeParentPathLength(quack_scope_list *ScopeList, quack_scope *Scope)
{
    s32 Result = 0;
    
    if(Scope)
    {
        while(FindScope(ScopeList, Scope->ScopeName))
        {
            Scope = Scope->Parent;
            ++Result;
            
            if(!Scope)
            {
                break;
            }
        }
    }
    
    return(Result);
}

quack_name *
FindQuackName(quack_scope *Scope, char *Name)
{
    quack_name *Result = 0;
    for(s32 Index = 0;
        Index < Scope->NameCount;
        ++Index)
    {
        if(CompareStrings(Name, Scope->Names[Index].Name))
        {
            Result = &Scope->Names[Index];
        }
    }
    
    return(Result);
}

b32
CheckScopeNameDuplicate(quack_scope *Scope, char *Name, s32 LineNumber)
{
    b32 Result = false;
    for(s32 Index = 0;
        Index < Scope->NameCount;
        ++Index)
    {
        if(CompareStrings(Name, Scope->Names[Index].Name))
        {
            Result = true;
        }
    }
    
    return(Result);
}

b32
CheckScopeNameDuplicateAndFollowParent(quack_scope *Scope, 
                                       char *Name, s32 LineNumber)
{
    b32 Result = false;
    
    for(;;)
    {
        if(Scope)
        {
            Result |= CheckScopeNameDuplicate(Scope, Name, LineNumber);
            Scope = Scope->Parent;
        }
        else
        {
            break;
        }
    }
    
    return(Result);
}

b32
GetFirstParentScopeWithName(quack_scope *Scope, 
                            char *Name)
{
    b32 Result = false;
    
    for(;;)
    {
        if(Scope)
        {
            Result = CheckScopeNameDuplicate(Scope, Name, LineNumber);
            if(Result)
            {
                break;
            }
            Scope = Scope->Parent;
        }
        else
        {
            break;
        }
    }
    
    return(Result);
}

b32
CheckScopeQuackNameDuplicate(quack_scope_list *ScopeList, quack_scope *Scope, 
                             quack_name *QuackName, s32 LineNumber)
{
    b32 Result = false;
    
    for(s32 Index = 0;
        Index < Scope->NameCount;
        ++Index)
    {
        if(CompareStrings(QuackName->Name, Scope->Names[Index].Name))
        {
            if(QuackName->ArgumentCount == Scope->Names[Index].ArgumentCount)
            {
                for(s32 ArgumentIndex = 0;
                    ArgumentIndex < QuackName->ArgumentCount;
                    ++ArgumentIndex)
                {
                    quack_name *QuackNameA = QuackName->ArgumentList[ArgumentIndex];
                    quack_name *QuackNameB = Scope->Names[Index].ArgumentList[ArgumentIndex];
                    
                    char *TypeA = QuackNameA->Type;
                    char *TypeB = QuackNameB->Type;
                    
                    if(CompareStrings(TypeA, TypeB))
                    {
                        
                    }
                    else
                    {
                        quack_scope *QuackScopeB = FindScope(ScopeList, TypeB);
                        
                        if(CheckScopeNameDuplicateAndFollowParent(QuackScopeB, TypeA, LineNumber))
                        {
                            
                        }
                        else
                        {
                            Result = true;
                            printf("%d: Illegal redefinition of %s, parameter types are not a superset\n", LineNumber, QuackName->Name);
                        }
                    }
                }
            }
            else
            {
                Result = true;
                printf("%d: Illegal redefinition of %s, number of parameters does not match\n", LineNumber, QuackName->Name);
            }
        }
    }
    
    return(Result);
}

b32
CheckScopeQuackNameDuplicateAndFollowParent(quack_scope_list *ScopeList, quack_scope *Scope, 
                                            quack_name *QuackName, s32 LineNumber)
{
    b32 Result = false;
    
    for(;;)
    {
        if(Scope)
        {
            Result = CheckScopeQuackNameDuplicate(ScopeList, Scope, QuackName, LineNumber);
            Scope = Scope->Parent;
            
            if(Result)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }
    
    return(Result);
}

void
AddName(quack_scope *Scope, char *Name)
{
    Scope->Names[Scope->NameCount].Name = Name;
    Scope->Names[Scope->NameCount].Type = 0;
    Scope->Names[Scope->NameCount].ReturnType = 0;
    Scope->Names[Scope->NameCount].ArgumentCount = 0;
    Scope->NameCount++;
}

void
AddQuackName(quack_scope *Scope, quack_name QuackName)
{
    Scope->Names[Scope->NameCount++] = QuackName;
}

b32
RequireSubtype(quack_scope *Scope, char *ParentType)
{
    b32 FoundParentWithType = false;
    b32 Loop = true;
    quack_scope *LoopScope = Scope;
    while(Loop)
    {
        if(LoopScope)
        {
            if(CompareStrings(LoopScope->ScopeName, ParentType))
            {
                FoundParentWithType= true;
            }
            
            LoopScope = LoopScope->Parent;
        }
        else
        {
            Loop = false;
        }
    }
    
    return(FoundParentWithType);
}

void
CheckForMatchingArgumentTypes2(node_ast *Node, 
                               s32 ListTargetChildIndex,
                               quack_scope *ResultScope,
                               quack_scope_list *ScopeList)
{
    s32 Index = 0;
    node_ast *ArgumentListNode = Node->NonTerminal.Children[ListTargetChildIndex];
    s32 ArgumentListSize = ArgumentListNode->List.List->size();
    if(ArgumentListSize != ResultScope->ArgumentCount)
    {
        printf("%d: Number of arguments do not match\n", Node->LineNumber);
    }
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = ArgumentListNode->List.List->rbegin();
        Iterator != ArgumentListNode->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *TypeNode = *Iterator;
            node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
            node_ast *TypeIdentNode = TypeNode->NonTerminal.Children[1];
            
            char *Name = NameIdentNode->Ident.Text;
            char *Type = TypeIdentNode->Ident.Text;
            
            if(Index < ResultScope->ArgumentCount)
            {
                char *CorrespondingClassArgumentType = ResultScope->Names[Index + 1].Type;
                quack_scope *CorrespondingScope = FindScope(ScopeList, 
                                                            CorrespondingClassArgumentType);
                if(CorrespondingScope)
                {
                    if(RequireSubtype(CorrespondingScope, Type))
                    {
                        
                    }
                    else
                    {
                        printf("%d: %s is not a supertype of the corresponding formal argument type %s\n",
                               NameIdentNode->LineNumber, Type, CorrespondingClassArgumentType);
                    }
                }
            }
            
            ++Index;
        }
    }
}

void
CheckForMatchingArgumentTypes(node_ast *Node, 
                              quack_scope *ResultScope,
                              quack_scope_list *ScopeList,
                              quack_scope *CurrentScope,
                              quack_scope *CurrentMethodScope,
                              s32 Pass,
                              s32 ListTargetChildIndex)
{
    quack_scope *Scope;
    if(CurrentMethodScope)
    {
        Scope = CurrentMethodScope;
    }
    else
    {
        Scope = CurrentScope;
    }
    
    s32 Index = 0;
    node_ast *ArgumentListNode = Node->NonTerminal.Children[ListTargetChildIndex];
    s32 ArgumentListSize = ArgumentListNode->List.List->size();
    
    if(ArgumentListSize != ResultScope->ArgumentCount)
    {
        printf("%d: Number of arguments do not match\n", Node->LineNumber);
    }
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = ArgumentListNode->List.List->rbegin();
        Iterator != ArgumentListNode->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            char *ArgumentType = 0;
            node_ast *AstNode = *Iterator;
            quack_scope *ArgumentRetrunedScope = TypeWalk(*Iterator, 
                                                          ScopeList, 
                                                          CurrentScope, 
                                                          Pass, 
                                                          CurrentMethodScope);
            
            if(ArgumentRetrunedScope)
            {
                ArgumentType = ArgumentRetrunedScope->ScopeName;
            }
            else
            {
                if(AstNode->AstType == AstType_ident)
                {
                    quack_name *ArgQuackName = FindQuackName(Scope, AstNode->Ident.Text);
                    if(ArgQuackName)
                    {
                        ArgumentType = ArgQuackName->Type;
                    }
                }
            }
            
            if(Index < ResultScope->ArgumentCount && ArgumentType)
            {
                char *CorrespondingClassArgumentType = ResultScope->Names[Index + 1].Type;
                quack_scope *CorrespondingScope = FindScope(ScopeList, 
                                                            CorrespondingClassArgumentType);
                quack_scope *ArgumentScope = FindScope(ScopeList, ArgumentType);
                
                if(ArgumentScope)
                {
                    if(CorrespondingScope)
                    {
                        if(RequireSubtype(ArgumentScope, CorrespondingScope->ScopeName))
                        {
                            
                        }
                        else
                        {
                            printf("%d: Argument type %s is not a subtype of the corresponding argument %s\n", AstNode->LineNumber, 
                                   ArgumentScope->ScopeName, 
                                   CorrespondingScope->ScopeName);
                        }
                    }
                }
                else
                {
                    printf("%d: Type %s for argument does not exist\n",
                           AstNode->LineNumber, ArgumentType);
                }
            }
            
            ++Index;
        }
    }
}

quack_scope *
LCA(quack_scope_list *ScopeList, quack_scope *ScopeX, quack_scope *ScopeY)
{
    quack_scope *Result = 0;
    
    s32 ScopeXParentPathLength = 
        ScopeParentPathLength(ScopeList, ScopeX);
    
    s32 ScopeYParentPathLength = 
        ScopeParentPathLength(ScopeList, ScopeY);
    
    quack_scope *YScopeBase = ScopeY;
    
    for(s32 X = 0;
        X < ScopeXParentPathLength;
        ++X)
    {
        ScopeY = YScopeBase;
        for(s32 Y = 0;
            Y < ScopeYParentPathLength;
            ++Y)
        {
            if(CompareStrings(ScopeX->ScopeName, ScopeY->ScopeName))
            {
                X = ScopeXParentPathLength;
                Y = ScopeYParentPathLength;
                
                Result = ScopeX;
            }
            
            ScopeY = ScopeY->Parent;
        }
        
        ScopeX = ScopeX->Parent;
    }
    
    return(Result);
}

b32 
ReturnPathExecCheck(node_ast *Node)
{
    b32 Result = false;
    if(Node->ListType == ListType_statement)
    {
        list<node_ast *>::reverse_iterator Iterator;
        for(Iterator = Node->List.List->rbegin();
            Iterator != Node->List.List->rend();
            ++Iterator)
        {
            if(*Iterator)
            {
                node_ast *NodeAst = *Iterator;
                
                if(NodeAst->NonTerminal.Type == NonTerminalType_return)
                {
                    printf("FOUND THE RETURN!!!\n");
                    Result = true;
                    break;
                }
                else if(NodeAst->NonTerminal.Type == NonTerminalType_if)
                {
                    node_ast *IfNode = NodeAst;
                    node_ast *ElifList = IfNode->NonTerminal.Children[2];
                    node_ast *OptionalElseNode = IfNode->NonTerminal.Children[3];
                    node_ast *StatementList = IfNode->NonTerminal.Children[1];
                    if(OptionalElseNode)
                    {
                        printf("OptionalElse\n");
                    }
                    if(IfNode)
                    {
                        printf("IF\n");
                    }
                    if(ElifList)
                    {
                        if(ElifList->List.List)
                        {
                            list<node_ast *>::reverse_iterator ElifIterator;
                            for(ElifIterator = ElifList->List.List->rbegin();
                                ElifIterator != ElifList->List.List->rend();
                                ++ElifIterator)
                            {
                                if(*ElifIterator)
                                {
                                    node_ast *ElifNode = *ElifIterator;
                                    node_ast *StatementList = ElifNode->NonTerminal.Children[1];
                                    if(ElifNode->NonTerminal.Type == NonTerminalType_elif)
                                    {
                                        
                                    }
                                    
                                    if(StatementList)
                                    {
                                        if(StatementList->ListType == ListType_statement)
                                        {
                                            
                                        }
                                        ReturnPathExecCheck(StatementList);
                                    }
                                    
                                }
                            }
                        }
                    }
                    
#if 0
                    if(ElifList)
                    {
                        //printf("asdfasdfffffffffffffffffffffffffffffffffffff\n");
                    }
                    else
                    {
                        //printf("|||||||||||||||||");
                    }
                    
                    if(ElifList->AstType)
                    {
                        //printf("my booooooones %s", ElifList->AstType);
                    }
                    
                    if(ElifList->List.List->size() > 0)
                    {
                        printf("List tpye: %s\n", ElifList->ListType);
                        if(OptionalElseNode->List.List)
                        {
                            printf("ALL 3\n");
                            b32 A = ReturnPathExecCheck(ElifList);
                            b32 B = ReturnPathExecCheck(OptionalElseNode);
                            b32 C = ReturnPathExecCheck(StatementList);
                            
                            b32 AllResults = A & B & C;
                            printf("%d, %d, %d, %d\n", A, B, C, AllResults);
                            
                            
                            Result = AllResults;
                            break;
                        }
                    }
#endif
                }
                else
                {
                    printf(".\n");
                }
            }
        }
    }
    
    return(Result);
}

quack_scope *
TypeWalk(node_ast *Node, quack_scope_list *ScopeList, quack_scope *CurrentScope, s32 Pass, 
         quack_scope *CurrentMethodScope)
{
    quack_scope *ResultScope = 0;
    
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class_signature:
                    {
                        if(Pass == 0)
                        {
                            node_ast *ClassIdentNode = Node->NonTerminal.Children[0];
                            char *ClassName = ClassIdentNode->Ident.Text;
                            
                            quack_scope *GlobalScope = FindScope(ScopeList, (char *)"Global");
                            if(CheckScopeNameDuplicate(GlobalScope, ClassName, ClassIdentNode->LineNumber))
                            {
                                printf("%d: Illegal redefinition of %s\n", 
                                       ClassIdentNode->LineNumber, ClassName);
                            }
                            
                            CurrentScope->Names[CurrentScope->NameCount++].Name = ClassName;
                            GlobalScope->Names[GlobalScope->NameCount++].Name = ClassName;
                            
                            if(Pass == 0)
                            {
                                quack_name QuackName = {};
                                QuackName.Name = ClassName;
                                QuackName.ArgumentCount = 0;
                                
                                s32 Index = 1;
                                node_ast *ClassArgumentListNode = Node->NonTerminal.Children[1];
                                list<node_ast *>::reverse_iterator Iterator;
                                for(Iterator = ClassArgumentListNode->List.List->rbegin();
                                    Iterator != ClassArgumentListNode->List.List->rend();
                                    ++Iterator)
                                {
                                    if(*Iterator)
                                    {
                                        node_ast *TypeNode = *Iterator;
                                        node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
                                        node_ast *TypeIdentNode = TypeNode->NonTerminal.Children[1];
                                        
                                        char *Name = NameIdentNode->Ident.Text;
                                        char *Type = TypeIdentNode->Ident.Text;
                                        
                                        quack_name NewQuackName = {};
                                        NewQuackName.Name = Name;
                                        NewQuackName.Type = Type;
                                        if(Index < 64)
                                        {
                                            CurrentScope->Names[Index].Name = NewQuackName.Name;
                                            CurrentScope->Names[Index].Type = NewQuackName.Type;
                                            CurrentScope->NameCount++;
                                            CurrentScope->ArgumentCount++;
                                        }
                                        else
                                        {
                                            printf("Too many arguments!\n");
                                        }
                                        
                                        ++Index;
                                    }
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_class:
                    {
                        node_ast *ClassSignatureNode = Node->NonTerminal.Children[0];
                        node_ast *ClassIdentNode = ClassSignatureNode->NonTerminal.Children[0];
                        char *ClassName = ClassIdentNode->Ident.Text;
                        if(Pass == 0)
                        {
                            quack_scope *AddedScope = AddScope(ScopeList, ClassName);
                        }
                        
                        CurrentScope = FindScope(ScopeList, ClassName);
                        
                        if(Pass == 0)
                        {
                            node_ast *ExtendIdentNode = ClassSignatureNode->NonTerminal.Children[2];
                            char *ExtendName = ExtendIdentNode->Ident.Text;
                            
                            quack_scope *ExtendScope = FindScope(ScopeList, ExtendName);
                            
                            if(ExtendScope)
                            {
                                CurrentScope->Parent = ExtendScope;
                            }
                            else
                            {
                                printf("%d: %s is extending a class %s that does not exist\n", ClassIdentNode->LineNumber, ClassName, ExtendName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_class_body:
                    {
                        
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        node_ast *MethodIdentNode = Node->NonTerminal.Children[0];
                        char *MethodName = MethodIdentNode->Ident.Text;
                        node_ast *MethodArgumentListNode = Node->NonTerminal.Children[1];
                        
                        if(Pass == 0)
                        {
                            quack_scope *AddedScope = AddScope(ScopeList, MethodName);
                            
                        }
                        
                        CurrentMethodScope = FindScope(ScopeList, MethodName);
                        
                        if(Pass == 0)
                        {
                            CurrentMethodScope->Names[CurrentMethodScope->NameCount++].Name = MethodName;
                            
                            quack_name QuackName = {};
                            QuackName.Name = MethodName;
                            QuackName.ArgumentCount = 0;
                            CurrentMethodScope->ClassScope = CurrentScope;
                            
                            s32 Index = 1;
                            list<node_ast *>::reverse_iterator Iterator;
                            for(Iterator = MethodArgumentListNode->List.List->rbegin();
                                Iterator != MethodArgumentListNode->List.List->rend();
                                ++Iterator)
                            {
                                if(*Iterator)
                                {
                                    node_ast *TypeNode = *Iterator;
                                    node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
                                    node_ast *TypeIdentNode = TypeNode->NonTerminal.Children[1];
                                    
                                    char *Name = NameIdentNode->Ident.Text;
                                    char *Type = TypeIdentNode->Ident.Text;
                                    
                                    quack_name NewQuackName = {};
                                    NewQuackName.Name = Name;
                                    NewQuackName.Type = Type;
                                    if(Index < 64)
                                    {
                                        CurrentMethodScope->Names[Index] = NewQuackName;
                                        CurrentMethodScope->ArgumentCount++;
                                        CurrentMethodScope->NameCount++;
                                    }
                                    else
                                    {
                                        printf("Too many arguments!\n");
                                    }
                                    
                                    ++Index;
                                }
                            }
                            
                            
                            node_ast *ReturnIdentNode = Node->NonTerminal.Children[2];
                            char *ReturnType = ReturnIdentNode->Ident.Text;
                            if(ReturnType)
                            {
                                CurrentMethodScope->Names[0].ReturnType = ReturnType;
                                
                                QuackName.ReturnType = ReturnType;
                                quack_scope *ReturnScope = FindScope(ScopeList, ReturnType);
                                if(ReturnScope)
                                {
                                    quack_scope *Scope = CurrentScope->Parent;
                                    if(GetFirstParentScopeWithName(Scope, QuackName.Name))
                                    {
                                        quack_name *QuackNameFromParent = FindQuackName(Scope, QuackName.Name);
                                        char *ParentReturnType = QuackNameFromParent->ReturnType;
                                        
                                        quack_scope *QuackScope = FindScope(ScopeList, ReturnType);
                                        
                                        if(QuackScope)
                                        {
                                            b32 FoundParentWithType = RequireSubtype(QuackScope, ParentReturnType);
                                            if(!FoundParentWithType)
                                            {
                                                printf("%d: Return type %s is not a subtype of %s\n", 
                                                       ReturnIdentNode->LineNumber,
                                                       ReturnType, ParentReturnType);
                                            }
                                        }
                                        
                                        quack_scope *ScopeWithClass = FindScopeWithClassScope(ScopeList, 
                                                                                              MethodName, 
                                                                                              Scope->ScopeName);
                                        if(ScopeWithClass)
                                        {
                                            CheckForMatchingArgumentTypes2(Node, 1, ScopeWithClass, ScopeList);
                                        }
                                    }
                                }
                                else
                                {
                                    printf("%d: Return type %s does not exist\n", 
                                           ReturnIdentNode->LineNumber, 
                                           ReturnType);
                                }
                            }
                            else
                            {
                                quack_scope *ReturnScope = FindScope(ScopeList, (char *)"Nothing");
                                CurrentMethodScope->Names[0].ReturnType = ReturnScope->ScopeName;
                            }
                            
                            if(CheckScopeNameDuplicate(CurrentScope, QuackName.Name, LineNumber))
                            {
                                printf("%d: Illegal redefinition of %s\n", LineNumber, QuackName.Name);
                            }
                            
                            quack_scope *NextScope = CurrentScope->Parent;
                            CheckScopeQuackNameDuplicateAndFollowParent(ScopeList, 
                                                                        NextScope, 
                                                                        &QuackName, 
                                                                        Node->LineNumber);
                            CurrentScope->Names[CurrentScope->NameCount++] = QuackName;
                        }
                    } break;
                    
                    case NonTerminalType_dot:
                    {
                        
                    } break;
                    
                    case NonTerminalType_program:
                    {
                        
                    } break;
                    
                    case NonTerminalType_statement:
                    {
                        
                    } break;
                    
                    case NonTerminalType_return:
                    {
                        
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        
                    } break;
                    
                    case NonTerminalType_type:
                    {
                        
                    } break;
                    
                    case NonTerminalType_accessor:
                    {
                        
                    } break;
                }
                
                b32 CastToBoolean = false;
                quack_scope *LeftScopeLCA = 0;
                quack_scope *OptionalTypeScope = 0;
                quack_scope *ScopeLCA = 0;
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        //printf("Currentscope: %s\n", CurrentScope->ScopeName);
                        quack_scope *ReturnedScope = TypeWalk(Node->NonTerminal.Children[Index], 
                                                              ScopeList, 
                                                              CurrentScope, 
                                                              Pass, 
                                                              CurrentMethodScope);
                        
                        switch(Node->NonTerminal.Type)
                        {
                            case NonTerminalType_return:
                            {
                                if(Pass == 0)
                                {
                                    if(ReturnedScope)
                                    {
                                        if(CurrentMethodScope)
                                        {
                                            quack_name *QuackName = FindQuackName(CurrentMethodScope,
                                                                                  CurrentMethodScope->ScopeName);
                                            if(QuackName->ReturnType)
                                            {
                                                if(CompareStrings(ReturnedScope->ScopeName,
                                                                  QuackName->ReturnType))
                                                {
                                                    
                                                }
                                                else
                                                {
                                                    printf("%d: Return type %s does not match the method return type %s\n",
                                                           Node->LineNumber,
                                                           ReturnedScope->ScopeName, 
                                                           QuackName->ReturnType);
                                                }
                                            }
                                        }
                                    }
                                }
                            } break;
                            
                            case NonTerminalType_if:
                            case NonTerminalType_while:
                            {
                                if(Index == 0)
                                {
                                    ScopeLCA = ReturnedScope;
                                }
                            } break;
                            
                            case NonTerminalType_elif:
                            {
                                if(Index == 0)
                                {
                                    ScopeLCA = ReturnedScope;
                                }
                            } break;
                            
                            case NonTerminalType_gets:
                            {
                                if(Index == 0)
                                {
                                    LeftScopeLCA = ReturnedScope;
                                }
                                
                                if(Index == 1)
                                {
                                    OptionalTypeScope = ReturnedScope;
                                }
                                
                                if(Index == 2)
                                {
                                    ScopeLCA = ReturnedScope;
                                    
                                    node_ast *LExpressionNode = Node->NonTerminal.Children[0];
                                    node_ast *LeftIdentNode = 0;
                                    if(LExpressionNode->AstType == AstType_ident)
                                    {
                                        LeftIdentNode = LExpressionNode;
                                    }
                                    
                                    if(LExpressionNode->AstType == AstType_non_terminal)
                                    {
                                        LeftIdentNode = LExpressionNode->NonTerminal.Children[1];
                                    }
                                    
                                    if(LeftIdentNode)
                                    {
                                        char *LeftIdentNodeName = LeftIdentNode->Ident.Text;
                                        quack_scope *TrueScope = 0;
                                        if(CurrentMethodScope)
                                        {
                                            TrueScope = CurrentMethodScope;
                                        }
                                        else
                                        {
                                            TrueScope = CurrentScope;
                                        }
                                        
                                        if(LeftScopeLCA && ScopeLCA)
                                        {
                                            b32 NotOverriding = true;
                                            for(s32 Index = 0;
                                                Index < (CurrentScope->ArgumentCount + 1);
                                                ++Index)
                                            {
                                                if(CompareStrings(CurrentScope->Names[Index].Name, 
                                                                  LeftIdentNodeName))
                                                {
                                                    if(LeftScopeLCA->IsAThis)
                                                    {
                                                        //printf("%d: Cannot override argument %s in method\n", 
                                                        //LeftIdentNode->LineNumber, LeftIdentNodeName);
                                                        //NotOverriding = false;
                                                        //LeftScopeLCA->IsAThis = false;
                                                    }
                                                }
                                            }
                                            
                                            //printf("%s << %s\n", LeftScopeLCA->ScopeName, ScopeLCA->ScopeName);
                                            quack_name *QuackNameToChange = FindQuackName(TrueScope, 
                                                                                          LeftIdentNodeName);
                                            
                                            if(QuackNameToChange && NotOverriding)
                                            {
                                                quack_scope *ResultLCA = LCA(ScopeList, 
                                                                             LeftScopeLCA, 
                                                                             ScopeLCA);
                                                if(ResultLCA)
                                                {
                                                    if(!CompareStrings(QuackNameToChange->Type,
                                                                       ResultLCA->ScopeName))
                                                    {
                                                        TypeBubbling = true;
                                                    }
                                                    
                                                    //printf("LCA %s\n", ResultLCA->ScopeName);
                                                    QuackNameToChange->Type = ResultLCA->ScopeName;
                                                }
                                            }
                                        }
                                        
                                        if(!LeftScopeLCA && ScopeLCA)
                                        {
                                            for(s32 Index = 0;
                                                Index < (CurrentScope->ArgumentCount + 1);
                                                ++Index)
                                            {
                                                if(CompareStrings(CurrentScope->Names[Index].Name, 
                                                                  LeftIdentNodeName))
                                                {
                                                    //printf("%d: Cannot overrrrrride argument %s in method\n", 
                                                    //LeftIdentNode->LineNumber, LeftIdentNodeName);
                                                }
                                            }
                                            
                                            quack_name NewQuackName = {};
                                            NewQuackName.Name = LeftIdentNodeName;
                                            NewQuackName.Type = ScopeLCA->ScopeName;
                                            AddQuackName(TrueScope, NewQuackName);
                                            TypeBubbling = true;
                                        }
                                        
                                        node_ast *OptionalTypeNode = Node->NonTerminal.Children[1];
                                        char *OptionalTypeName = OptionalTypeNode->Ident.Text;
                                        if(OptionalTypeName)
                                        {
                                            quack_scope *OptionalScope = FindScope(ScopeList, OptionalTypeName);
                                            if(OptionalScope)
                                            {
                                                if(!OptionalScope->Names[0].ReturnType)
                                                {
                                                    quack_name *ChangedQuackName = FindQuackName(TrueScope, 
                                                                                                 LeftIdentNodeName);
                                                    quack_scope *ChangedQuackNameScope = FindScope(ScopeList,
                                                                                                   ChangedQuackName->Type);
                                                    quack_scope *OptionalScope = FindScope(ScopeList, 
                                                                                           OptionalTypeName);
                                                    if(ChangedQuackName && OptionalScope && ChangedQuackNameScope)
                                                    {
                                                        if(RequireSubtype(ChangedQuackNameScope, 
                                                                          OptionalTypeName))
                                                        {
                                                            if(!CompareStrings(ChangedQuackName->Type,
                                                                               OptionalTypeName))
                                                            {
                                                                TypeBubbling = true;
                                                            }
                                                            
                                                            ChangedQuackName->Type = OptionalTypeName;
                                                        }
                                                        else
                                                        {
                                                            printf("%d: formal type %s is not a supertype of %s\n",
                                                                   OptionalTypeNode->LineNumber, 
                                                                   OptionalTypeName,
                                                                   ChangedQuackName->Type);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    printf("%d: Formal type must be a class\n",
                                                           OptionalTypeNode->LineNumber);
                                                }
                                            }
                                            
#if 0
                                            if(RequireSubtype(ScopeLCA, OptionalTypeScope->ScopeName))
                                            {
                                                //quack_name *VarQuackName = FindQuackName(Scope, VarName);
                                                //VarQuackName->Type = OptionalTypeScope->ScopeName;
                                            }
                                            else
                                            {
                                                printf("%d: %s is not a subtype of the assignemnt type %s\n",
                                                       Node->LineNumber, 
                                                       OptionalTypeScope->ScopeName, 
                                                       ScopeLCA->ScopeName);
                                            }
#endif
                                        }
                                    }
                                }
                            } break;
                            
                            case NonTerminalType_dot:
                            {
                                if(Index == 0)
                                {
                                    node_ast *PossibleIdentNode = Node->NonTerminal.Children[0];
                                    if(PossibleIdentNode)
                                    {
                                        if(PossibleIdentNode->AstType == AstType_ident)
                                        {
                                            char *IdentNodeText = PossibleIdentNode->Ident.Text;
                                            if(IdentNodeText)
                                            {
                                                if(CompareStrings(IdentNodeText, (char *)"this"))
                                                {
                                                    if(!CompareStrings(CurrentScope->ScopeName, 
                                                                       (char *)"Global"))
                                                    {
                                                        ReturnedScope = CurrentScope;
                                                        ReturnedScope->IsAThis = true;
                                                    }
                                                    else
                                                    {
                                                        ReturnedScope->IsAThis = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(ReturnedScope)
                                    {
                                        node_ast *VarNode = Node->NonTerminal.Children[1];
                                        char *VarName = VarNode->Ident.Text;
                                        quack_name *QuackName = FindQuackName(ReturnedScope, VarName);
                                        if(QuackName)
                                        {
                                            if(QuackName->ReturnType)
                                            {
                                                printf("%d: %s is a function not a variable\n", 
                                                       VarNode->LineNumber, VarName);
                                            }
                                            else
                                            {
                                                if(QuackName->Type)
                                                {
                                                    quack_scope *VarScope = FindScope(ScopeList, QuackName->Type);
                                                    if(VarScope)
                                                    {
                                                        ScopeLCA = VarScope;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if(!ReturnedScope->IsAThis)
                                            {
                                                printf("%d: %s does not exist in class %s\n", 
                                                       VarNode->LineNumber, VarName, ReturnedScope->ScopeName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        printf("%d: To the right of \".\" does not exist\n", 
                                               Node->LineNumber);
                                    }
                                }
                            } break;
                            
                            case NonTerminalType_accessor:
                            {
                                if(Index == 0)
                                {
                                    ScopeLCA = ReturnedScope;
                                    
                                    if(ScopeLCA)
                                    {
                                        quack_scope *ClassScope = 0;
                                        quack_name *QuackName = 0;
                                        node_ast *MethodIdentNode = Node->NonTerminal.Children[1];
                                        char *MethodName = MethodIdentNode->Ident.Text;
                                        
                                        quack_scope *SearchScope = FindScopeWithClassScope(ScopeList,
                                                                                           MethodName,
                                                                                           ReturnedScope->ScopeName);
                                        
                                        if(SearchScope)
                                        {
                                            CheckForMatchingArgumentTypes(Node,
                                                                          SearchScope,
                                                                          ScopeList,
                                                                          CurrentScope,
                                                                          CurrentMethodScope,
                                                                          Pass, 2);
                                            quack_name *SearchQuackName = FindQuackName(SearchScope, MethodName);
                                            
                                            if(SearchQuackName)
                                            {
                                                ScopeLCA = FindScope(ScopeList, SearchQuackName->ReturnType);
                                            }
                                        }
                                        else
                                        {
                                            printf("%d: Method %s does not exist in scope %s\n",
                                                   MethodIdentNode->LineNumber, MethodName, 
                                                   ReturnedScope->ScopeName);
                                        }
                                    }
                                }
                            } break;
                            
                            case NonTerminalType_constructor:
                            {
                                if(Index == 0)
                                {
                                    node_ast *IdentNode = Node->NonTerminal.Children[0];
                                    char *IdentNodeText = IdentNode->Ident.Text;
                                    quack_scope *ConstructorScope = FindScope(ScopeList, IdentNodeText);
                                    
                                    if(ConstructorScope)
                                    {
                                        ScopeLCA = ConstructorScope;
                                    }
                                    else
                                    {
                                        printf("%d: There is no class with constructor %s\n", 
                                               IdentNode->LineNumber, IdentNodeText);
                                    }
                                }
                            } break;
                            
                            default:
                            {
                                if(ReturnedScope)
                                {
                                    if(ScopeLCA)
                                    {
                                        s32 ReturnedScopeParentPathLength = 
                                            ScopeParentPathLength(ScopeList, ReturnedScope);
                                        
                                        s32 ScopeLCAParentPathLength = 
                                            ScopeParentPathLength(ScopeList, ScopeLCA);
                                        
                                        quack_scope *ScopeX = ReturnedScope;
                                        for(s32 X = 0;
                                            X < ReturnedScopeParentPathLength;
                                            ++X)
                                        {
                                            quack_scope *ScopeY = ScopeLCA;
                                            for(s32 Y = 0;
                                                Y < ScopeLCAParentPathLength;
                                                ++Y)
                                            {
                                                if(CompareStrings(ScopeX->ScopeName, ScopeY->ScopeName))
                                                {
                                                    X = ReturnedScopeParentPathLength;
                                                    Y = ScopeLCAParentPathLength;
                                                    
                                                    ScopeLCA = ScopeX;
                                                }
                                                
                                                ScopeY = ScopeY->Parent;
                                            }
                                            
                                            ScopeX = ScopeX->Parent;
                                        }
                                    }
                                    else
                                    {
                                        ScopeLCA = ReturnedScope;
                                    }
                                    
                                    switch(Node->NonTerminal.Type)
                                    {
                                        case NonTerminalType_equals:
                                        {
                                            ScopeLCA = FindScope(ScopeList, (char *)"Boolean");
                                        } break;
                                        
                                        case NonTerminalType_atmost:
                                        case NonTerminalType_less:
                                        case NonTerminalType_atleast:
                                        case NonTerminalType_more:
                                        {
                                            if(RequireSubtype(ScopeLCA, (char *)"Integer"))
                                            {
                                                CastToBoolean = true;
                                            }
                                            else if(RequireSubtype(ScopeLCA, (char *)"String"))
                                            {
                                                CastToBoolean = true;
                                            }
                                            else
                                            {
                                                printf("MOOORE\n");
                                                char *OperatorForType = (char *)"";
                                                switch(Node->NonTerminal.Type)
                                                {
                                                    case NonTerminalType_atmost: {OperatorForType = (char *)"<=";} break;
                                                    case NonTerminalType_less: {OperatorForType = (char *)"<";} break;
                                                    case NonTerminalType_atleast: {OperatorForType = (char *)">=";} break;
                                                    case NonTerminalType_more: {OperatorForType = (char *)">";} break;
                                                }
                                                
                                                printf("%d: The \"%s\" operator requires a subtype of Integer or Boolean and was passed a type %s\n", 
                                                       Node->LineNumber, 
                                                       OperatorForType, 
                                                       ScopeLCA->ScopeName);
                                            }
                                        } break;
                                    }
                                }
                            } break;
                        }
                    }
                }
                
                if(CastToBoolean)
                {
                    ScopeLCA = FindScope(ScopeList, (char *)"Boolean");
                }
                
                ResultScope = ScopeLCA;
                
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_constructor:
                    {
                        if(ResultScope)
                        {
                            CheckForMatchingArgumentTypes(Node,
                                                          ResultScope,
                                                          ScopeList,
                                                          CurrentScope,
                                                          CurrentMethodScope,
                                                          Pass, 1);
                        }
                    } break;
#if 0
                    case NonTerminalType_accessor:
                    {
                        node_ast *MethodIdentNode = Node->NonTerminal.Children[1];
                        char *MethodName = MethodIdentNode->Ident.Text;
                        quack_scope *ClassScope = 0;
                        quack_name *QuackName = 0;
                        if(ResultScope)
                        {
                            // SWITCH SO THTAT foo.x() acutally evaluates to the return type of foo
                            // it will mess  this up but it shouldn't be a big deal maybe??????
                            QuackName = FindQuackName(ResultScope, MethodName);
                            if(QuackName)
                            {
                                ClassScope = ResultScope;
                            }
                            else
                            {
                                printf("%d: %s does not exist in class %s\n", 
                                       MethodIdentNode->LineNumber, MethodName, ResultScope->ScopeName);
                            }
                        }
                        else
                        {
                            if(CurrentMethodScope)
                            {
                                QuackName = FindQuackName(CurrentMethodScope, MethodName);
                                if(QuackName)
                                {
                                    ClassScope = CurrentMethodScope;
                                }
                                else
                                {
                                    printf("%d: %s does not exist in %s\n", 
                                           MethodIdentNode->LineNumber, MethodName, CurrentMethodScope->ScopeName);
                                }
                            }
                            else
                            {
                                QuackName = FindQuackName(CurrentScope, MethodName);
                                if(QuackName)
                                {
                                    ClassScope = CurrentScope;
                                }
                                else
                                {
                                    printf("%d: %s does not exist in class %s\n", 
                                           MethodIdentNode->LineNumber, MethodName, CurrentScope->ScopeName);
                                }
                            }
                        }
                        
                        if(ResultScope)
                        {
                            if(QuackName)
                            {
                                if(QuackName->ReturnType)
                                {
                                    
                                    quack_scope *SearchScope = FindScopeWithClassScope(ScopeList, 
                                                                                       MethodName,
                                                                                       ClassScope->ScopeName);
                                    
                                    if(SearchScope)
                                    {
#if 1
                                        CheckForMatchingArgumentTypes(Node,
                                                                      SearchScope,
                                                                      ScopeList,
                                                                      CurrentScope,
                                                                      CurrentMethodScope,
                                                                      Pass, 2);
                                        quack_name *SearchQuackName = FindQuackName(SearchScope, 
                                                                                    SearchScope->ScopeName);
                                        if(SearchQuackName && SearchQuackName->ReturnType)
                                        {
                                            quack_scope *VarScope = FindScope(ScopeList, 
                                                                              SearchQuackName->ReturnType);
                                            if(VarScope)
                                            {
                                                //ScopeLCA = VarScope;
                                            }
                                        }
#endif
                                    }
                                }
                                else
                                {
                                    printf("%d: %s is not a method\n", 
                                           MethodIdentNode->LineNumber, MethodName);
                                }
                            }
                            else
                            {
                                
                            }
                        }
                    } break;
#endif
                    
                    case NonTerminalType_if:
                    {
                        if(ResultScope)
                        {
                            if(!RequireSubtype(ResultScope, (char *)"Boolean"))
                            {
                                printf("%d: \"If\" statements require a test of subtype Boolean and the test was passed a type %s\n", Node->LineNumber, ResultScope->ScopeName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_elif:
                    {
                        if(ResultScope)
                        {
                            if(!RequireSubtype(ResultScope, (char *)"Boolean"))
                            {
                                printf("%d: \"Elif\" statements require a test of subtype Boolean and the test was passed a type %s\n", Node->LineNumber, ResultScope->ScopeName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        if(ResultScope)
                        {
                            if(!RequireSubtype(ResultScope, (char *)"Boolean"))
                            {
                                printf("%d: \"While\" statements require a test of subtype Boolean and the test was passed a type %s", Node->LineNumber, ResultScope->ScopeName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_plus:
                    case NonTerminalType_sub:
                    case NonTerminalType_mult:
                    case NonTerminalType_div:
                    case NonTerminalType_unary:
                    {
                        if(ResultScope)
                        {
                            if(RequireSubtype(ResultScope, (char *)"Integer"))
                            {
                                
                            }
                            else
                            {
                                char *OperatorForType = (char *)"";
                                switch(Node->NonTerminal.Type)
                                {
                                    case NonTerminalType_plus: {OperatorForType = (char *)"+";} break;
                                    case NonTerminalType_sub: {OperatorForType = (char *)"-";} break;
                                    case NonTerminalType_mult: {OperatorForType = (char *)"*";} break;
                                    case NonTerminalType_div: {OperatorForType = (char *)"/";} break;
                                    case NonTerminalType_unary: {OperatorForType = (char *)"unary minus";} break;
                                }
                                
                                printf("%d: The \"%s\" operator requires a subtype of Integer and was passed a type %s\n", 
                                       Node->LineNumber, 
                                       OperatorForType, 
                                       ResultScope->ScopeName);
                            }
                            
                            if(Pass == 1)
                            {
                                
                            }
                        }
                    } break;
                    
                    case NonTerminalType_and:
                    case NonTerminalType_or:
                    case NonTerminalType_not:
                    {
                        if(ResultScope)
                        {
                            if(RequireSubtype(ResultScope, (char *)"Boolean"))
                            {
                                
                            }
                            else
                            {
                                char *OperatorForType = (char *)"";
                                switch(Node->NonTerminal.Type)
                                {
                                    case NonTerminalType_and: {OperatorForType = (char *)"AND";} break;
                                    case NonTerminalType_or: {OperatorForType = (char *)"OR";} break;
                                    case NonTerminalType_not: {OperatorForType = (char *)"NOT";} break;
                                }
                                
                                printf("%d: The \"%s\" operator requires a subtype of Boolean and was passed a type %s\n", 
                                       Node->LineNumber, 
                                       OperatorForType, 
                                       ResultScope->ScopeName);
                            }
                        }
                    } break;
                }
            } break;
            
            case AstType_string_lit:
            {
                ResultScope = FindScope(ScopeList, (char *)"String");
            } break;
            
            case AstType_int_lit:
            {
                ResultScope = FindScope(ScopeList, (char *)"Integer");
            } break;
            
            case AstType_ident:
            {
                char *IdentText = Node->Ident.Text;
                if(IdentText)
                {
                    quack_scope *Scope = 0;
                    if(CurrentMethodScope)
                    {
                        Scope = CurrentMethodScope;
                    }
                    else
                    {
                        Scope = CurrentScope;
                    }
                    
                    if(Scope)
                    {
                        quack_name *QuackName = FindQuackName(Scope, IdentText);
                        if(QuackName)
                        {
                            if(QuackName->Type)
                            {
                                quack_scope *TypeScope = FindScope(ScopeList, QuackName->Type);
                                if(TypeScope)
                                {
                                    ResultScope = TypeScope;
                                }
                            }
                        }
                        else
                        {
                            //printf("Can't find quack name %s\n", IdentText);
                        }
                        //ResultScope = FindScope(ScopeList, IdentText);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        TypeWalk(*Iterator, ScopeList, CurrentScope, Pass, 
                                 CurrentMethodScope);
                    }
                }
                
                if(Node->ListType == ListType_method)
                {
                    CurrentMethodScope = 0;
                }
            } break;
        }
    }
    
    return(ResultScope);
}

int
main(int argc, char **argv)
{
#if 0
#ifdef YYDEBUG
    extern int yydebug;
    yydebug = 1;
#endif
#endif
    
    if(argc > 1)
    {
        FILE *File = fopen(argv[1], "r");
        
        if(File)
        {
            //printf("Beginning parse of %s\n", argv[1]);
            yyin = File;
            int Result = yyparse();
            
            if(Result ==  0)
            {
                //printf("Finished parse with no errors\n");
            }
            else
            {
                printf("Finished parse with some errors\n");
            }
            
            list<node_ast *> *ClassList = RootNode->NonTerminal.Children[0]->List.List;
            
            b32 ConstructorsWellFormed = true;
            //WalkAst2(RootNode, 0, ClassList, &ConstructorsWellFormed);
            WalkAst(RootNode, ClassList, &ConstructorsWellFormed);
            
            if(ConstructorsWellFormed)
            {
                //printf("Constructor calls are well formed.\n");
            }
            else
            {
                printf("Constructor calls are NOT well formed.\n");
            }
            
            b32 IsWellFormed = true;
            list<node_ast *>::const_iterator Iterator;
            for(Iterator = ClassList->begin();
                Iterator != ClassList->end();
                ++Iterator)
            {
                if(*Iterator)
                {
                    ClearVisits(ClassList);
                    
                    node_ast *Node = (*Iterator)->NonTerminal.Children[0];
                    Node->Visited = true;
                    char *BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                    char *ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                    
                    for(;;)
                    {
                        if(FindClassNode(ExtendName, ClassList))
                        {
                            Node = FindClassNode(ExtendName, ClassList);
                            BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                            ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            
                            if(Node->Visited)
                            {
                                IsWellFormed = false;
                                break;
                            }
                            
                            Node->Visited = true;
                            if(Node->NonTerminal.Children[2]->Ident.Text)
                            {
                                ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            }
                            else
                            {
                                ExtendName = 0;
                            }
                        }
                        else
                        {
                            char Obj[] = "Obj";
                            if(!CompareStrings(ExtendName, Obj))
                            {
                                IsWellFormed = false;
                            }
                            
                            break;
                        }
                    }
                }
            }
            
            if(!IsWellFormed)
            {
                printf("Class hierarchy is NOT well formed.\n");
            }
            else
            {
                //printf("Class hierarchy is well formed!\n");
            }
            
            quack_scope_list ScopeList = {};
            quack_scope *GlobalScope = AddScope(&ScopeList, (char *)"Global");
            quack_scope *ObjScope = AddScope(&ScopeList, (char *)"Obj");
            ObjScope->Parent = 0;
            ObjScope->Names[ObjScope->NameCount++].Name = (char *)"Obj";
            ObjScope->Names[ObjScope->NameCount++].Name = (char *)"Print";
            ObjScope->Names[1].ReturnType = (char *)"Nothing";
            
            AddScope(&ScopeList, (char *)"Integer")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"String")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"Boolean")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"true")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"false")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"and")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"or")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"not")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"Nothing")->Parent = ObjScope;
            AddScope(&ScopeList, (char *)"none")->Parent = ObjScope;
            
            if(IsWellFormed)
            {
                b32 BubbleLoop = true;
                s32 BubbleLoopCount = 0;
                s32 BubbleLoopMax = 100;
                while(BubbleLoop)
                {
                    TypeBubbling = false;
                    TypeWalk(RootNode, &ScopeList, GlobalScope, BubbleLoopCount, 0);
                    
                    BubbleLoop = TypeBubbling;
                    ++BubbleLoopCount;
                    if(BubbleLoopCount > BubbleLoopMax)
                    {
                        BubbleLoop = false;
                    }
                }
                
                //printf("DONE in %d bubbles\n", BubbleLoopCount);
            }
        }
        else
        {
            printf("Could not read quack file\n");
        }
    }
    else
    {
        printf("No file was passed\n");
        yyparse();
    }
    
    return(0);
}
