%{
#include <string.h>

#define YY_DECL extern "C" int yylex()
#include "quack.tab.h"

int LineNumber = 1;

void
yyerror(const char *Text)
{
    fprintf(stderr, "%s on line #%d \n", Text, LineNumber);
    exit(-1);
}

%}

%option yylineno
%x COMMENT
%x TRIPLEQUOTE

%%

[-+*/():{};=.,] return yytext[0];

"==" return EQUALS;
"<=" return ATMOST;
"<" return LESS;
">=" return ATLEAST;
">" return MORE;

"class" return CLASS;
"extends" return EXTENDS;
"def" return DEF;
"if" return IF;
"elif" return ELIF;
"else" return ELSE;
"return" return RETURN;
"while" return WHILE;
"and" return AND;
"or" return OR;
"not" return NOT;

[a-zA-Z_][a-zA-Z0-9_]* { yylval.Text = strdup(yytext); return IDENT; }
[0-9]+ { char S[] = "string_lit placeholder"; yylval.Text = S; return INT_LIT; }

	/* Triple quote system from the sample in class as it is much more elagant than my original implementation */

["]["]["] BEGIN(TRIPLEQUOTE);
<TRIPLEQUOTE>(([^"])|(["][^"])|(["]["][^"])|\n)* { ; }
<TRIPLEQUOTE>["]["]["] { BEGIN(INITIAL);  char S[] = "string_lit placeholder"; yylval.Text = S; return STRING_LIT; }

"/*" BEGIN(COMMENT);
<COMMENT>\n { ++LineNumber; }
<COMMENT>. ;
<COMMENT>"*/" BEGIN(INITIAL);

["](([\\][0btnrf"\\])|[^"\\\n])*["] { char S[] = "string_lit placeholder"; yylval.Text = S; return STRING_LIT; }

[/][/].* ;

[ \t] ;
\n ++LineNumber;

. printf("unknown character\n");

%%

extern int yywrap(void) 
{
	return(1);
}