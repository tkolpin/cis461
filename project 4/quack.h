#if !defined(QUACK_H)

#include <list>
#include <stdarg.h>
#include <stdint.h>

using namespace std;

typedef __SIZE_TYPE__ memory_index;

typedef int8_t s08;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef s32 b32;

typedef uint8_t u08;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;

#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

enum node_type
{
    NodeType_empty,
    NodeType_class,
    NodeType_ident,
};

struct node_class
{
    node_type Type;
    char *Name;
    char *ExtendName;
    b32 Visited;
};

struct node_ident
{
    node_type Type;
    char *Text;
};

struct node_int_lit
{
    node_type Type;
    char *Text;
};

struct node_string_lit
{
    node_type Type;
    char *Text;
};

enum non_terminal_type
{
    NonTerminalType_plus,
    NonTerminalType_elif,
    NonTerminalType_sub,
    NonTerminalType_mult,
    NonTerminalType_div,
    NonTerminalType_unary,
    NonTerminalType_equals,
    NonTerminalType_atmost,
    NonTerminalType_less,
    NonTerminalType_atleast,
    NonTerminalType_more,
    NonTerminalType_and,
    NonTerminalType_or,
    NonTerminalType_not,
    NonTerminalType_constructor,
    NonTerminalType_method,
    NonTerminalType_dot,
    NonTerminalType_program,
    NonTerminalType_class,
    NonTerminalType_statement,
    NonTerminalType_if,
    NonTerminalType_gets,
    NonTerminalType_return,
    NonTerminalType_while,
    NonTerminalType_class_signature,
    NonTerminalType_type,
    NonTerminalType_class_body,
    NonTerminalType_accessor,
    NonTerminalType_rexpression,
};

enum ast_type
{
    AstType_non_terminal,
    AstType_non_terminal_list,
    AstType_ident,
    AstType_int_lit,
    AstType_string_lit,
};

enum list_type
{
    ListType_empty,
    ListType_statement,
    ListType_class,
    ListType_elif,
    ListType_argument,
    ListType_actual_argument,
    ListType_method,
};

struct node_ast;

struct node_non_terminal
{
    non_terminal_type Type;
    
    s32 ChildCount;
    node_ast *Children[32];
};

struct node_list
{
    list<node_ast *> *List;
};

struct node_ast
{
    ast_type AstType;
    
    union
    {
        node_non_terminal NonTerminal;
        node_ident Ident;
        node_int_lit IntLit;
        node_string_lit StringLit;
    };
    
    list_type ListType;
    node_list List;
    
    b32 Visited;
    
    s32 LineNumber;
};

struct quack_scope;

struct quack_name
{
    char *Name;
    char *Type;
    char *ReturnType;
    quack_name *ArgumentList[64];
    s32 ArgumentCount;
    quack_scope *AccessorScope;
};

struct quack_scope
{
    char *ScopeName;
    quack_name Names[1024];
    s32 NameCount;
    
    quack_scope *Parent;
    s32 ArgumentCount;
    quack_scope *ClassScope;
    b32 IsAThis;
};

struct quack_scope_list
{
    quack_scope *List[1024];
    s32 ListSize;
};

node_ast *NewNonTerminalNode(s32 LineNumber, non_terminal_type NonTerminalType, s32 ChildCount, ...);
node_ast *NewIdentNode(s32 LineNumber, char *Name);
node_ast *NewIntLitNode(s32 LineNumber, char *Name);
node_ast *NewStringLitNode(s32 LineNumber, char *Name);
node_ast *NewListNode(list_type ListType);

quack_scope *
TypeWalk(node_ast *Node, quack_scope_list *ScopeList, quack_scope *CurrentScope, s32 Pass, 
         quack_scope *CurrentMethodScope);

#define QUACK_H
#endif