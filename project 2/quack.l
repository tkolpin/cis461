%{
#define YY_DECL extern "C" int yylex()

#include "quack.tab.h"

int LineNumber = 1;

%}

%x COMMENT
%x TRIPLEQUOTE

%%

[-+*/():{};=.,] return yytext[0];

"==" return EQUALS;
"<=" return ATMOST;
"<" return LESS;
">=" return ATLEAST;
">" return MORE;

"class" return CLASS;
"extends" return EXTENDS;
"def" return DEF;
"if" return IF;
"elif" return ELIF;
"else" return ELSE;
"return" return RETURN;
"while" return WHILE;
"and" return AND;
"or" return OR;
"not" return NOT;

[a-zA-Z_][a-zA-Z0-9_]* { yylval.StringValue = yytext; return IDENT; }
[0-9]+ return INT_LIT;

	/* Triple quote system from the sample in class as it is much more elagant than my original implementation */

["]["]["] BEGIN(TRIPLEQUOTE);
<TRIPLEQUOTE>(([^"])|(["][^"])|(["]["][^"])|\n)* { ; }
<TRIPLEQUOTE>["]["]["] { BEGIN(INITIAL); return STRING_LIT; }

"/*" BEGIN(COMMENT);
<COMMENT>\n { ++LineNumber; }
<COMMENT>. ;
<COMMENT>"*/" BEGIN(INITIAL);

["](([\\][0btnrf"\\])|[^"\\\n])*["] return STRING_LIT;

[/][/].* ;

[ \t] ;
\n ++LineNumber;

. printf("unknown character\n");

%%

extern int yywrap(void) 
{
	return(1);
}