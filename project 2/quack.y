%{
#include <stdio.h>

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int LineNumber;

void yyerror(const char *Text);
%}

%error-verbose

%union
{
	char *StringValue;
}

%start Program

%token CLASS
%token DEF
%token EXTENDS
%token IF
%token ELIF
%token ELSE
%token WHILE
%token RETURN
%token <StringValue> IDENT
%token INT_LIT
%token STRING_LIT
%token EQUALS;
%token ATMOST;
%token LESS;
%token ATLEAST;
%token MORE;

%left AND
%left OR
%nonassoc EQUALS ATMOST LESS ATLEAST MORE
%left '+' '-'
%left '*' '/'

%right UNARY NOT

%left '.'

%%

Program:
	ClassList StatementList { ; }
	;

ClassList:
	Class ClassList { ; }
	| /* empty */ { ; }
	;

StatementList:
	Statement StatementList { ; }
	| /* empty */ { ; }
	;

Statement:
	IF RExpression StatementBlock ElifList OptionalElse { ; }
	| WHILE RExpression StatementBlock { ; }
	| RETURN OptionalRExpression ';' { ; }
	| LExpression OptionalType '=' RExpression ';' { ; }
	| RExpression ';' { ; }
	;

ElifList:
	ELIF RExpression StatementBlock ElifList { ; }
	| /* empty */ { ; }
	;

OptionalElse:
	ELSE StatementBlock{ ; }
	| /* empty */ { ; }
	;

OptionalRExpression:
	RExpression { ; }
	| /* empty */ { ; }
	;

RExpression:
	STRING_LIT { ; }
	| INT_LIT { ; }
	| LExpression { ; }
	| RExpression '+' RExpression { ; }
	| RExpression '-' RExpression { ; }
	| RExpression '*' RExpression { ; }
	| RExpression '/' RExpression { ; }
	| '-' RExpression %prec UNARY { ; }
	| '(' RExpression ')' { ; }
	| RExpression EQUALS RExpression { ; }
	| RExpression ATMOST RExpression { ; }
	| RExpression LESS RExpression { ; }
	| RExpression ATLEAST RExpression { ; }
	| RExpression MORE RExpression { ; }
	| RExpression AND RExpression { ; }
	| RExpression OR RExpression { ; }
	| NOT RExpression { ; }
	| IDENT '(' ActualArgumentList ')' { ; }
	| RExpression '.' IDENT '(' ActualArgumentList  ')' { ; }
	;

LExpression:
	IDENT { ; }
	| RExpression '.' IDENT { ; }
	;

ActualArgumentList:
	NonEmptyActualArgumentList { ; }
	| /* empty */ { ; }
	;

NonEmptyActualArgumentList:
	ActualArgument { ; }
	| NonEmptyActualArgumentList ',' ActualArgument { ; }
	;

ActualArgument:
	RExpression { ; }
	;

Class:
	ClassSignature ClassBody { ; }
	;

ClassSignature:
	CLASS IDENT '(' ArgumentList ')' OptionalExtend { ; }
	;

ArgumentList:
	NonEmptyArgumentList { ; }
	| /* empty */ { ; }
	;

NonEmptyArgumentList:
	Argument { ; }
	| NonEmptyArgumentList ',' Argument { ; }
	;

Argument:
	IDENT ':' IDENT { ; }
	;

OptionalExtend:
	EXTENDS IDENT 
	| /* empty */ { ; }
	;

ClassBody:
	'{' StatementList MethodList '}' { ; }
	;

MethodList:
	Method MethodList { ; }
	| /* empty */ { ; }
	;

Method:
	DEF IDENT '(' ArgumentList ')' OptionalType StatementBlock { ; }
	;

OptionalType:
	':' IDENT { ; }
	| /* empty */ { ; }
	;

StatementBlock:
	'{' StatementList '}' { ; }
	;

%%

int
main(int argc, char **argv)
{
#if 0
#ifdef YYDEBUG
	extern int yydebug;
	yydebug = 1;
#endif
#endif
	
	if(argc > 1)
	{
		FILE *File = fopen(argv[1], "r");

		if(File)
		{
			printf("Beginning parse of %s\n", argv[1]);
			yyin = File;
			int Result = yyparse();
			
			printf("Finished parse with no errors\n");
		}
		else
		{
			printf("Could not read quack file\n");
		}
	}
	else
	{
		printf("No file was passed\n");
		yyparse();
	}

	return(0);
}

void
yyerror(const char *Text)
{
	fprintf(stderr, "%d: %s\n", LineNumber, Text);
	exit(-1);
}