#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#include "quack.h"

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int LineNumber;

void yyerror(const char *Text);

node_ast *RootNode;
node_ast *GlobalContext;

char Obj[] = "Obj";
char Nothing[] = "Nothing";

s32 TmpCounter;
b32 VarBubbling;
b32 VarBubblingDone;
s32 ErrorCount;
#define MaxErrorCount 10

node_ast *
NewListNode(list_type ListType)
{
    node_ast *Result;
    
    node_ast *NodeAst = new node_ast;
    NodeAst->List.List = new list<node_ast *>();
    NodeAst->ListType = ListType;
    NodeAst->AstType = AstType_non_terminal_list;
    NodeAst->LineNumber = LineNumber;
    
    Result = NodeAst;
#if 0
    printf("Creating List Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal_list, Result->AstType);
    printf("ListType: %d ResultType: %d\n", ListType, Result->ListType);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewNonTerminalNode(non_terminal_type NonTerminalType, s32 ChildCount, ...)
{
    node_ast *Result = new node_ast;
    if(Result)
    {
        Result->AstType = AstType_non_terminal;
        Result->ListType = ListType_empty;
        Result->NonTerminal.Type = NonTerminalType;
        Result->NonTerminal.ChildCount = ChildCount;
        Result->LineNumber = LineNumber;
        
        list<node_ast *> *List;
        
        va_list VaList;
        va_start(VaList, ChildCount);
        for(s32 Index = 0;
            Index < ChildCount;
            ++Index)
        {
            Result->NonTerminal.Children[Index] = va_arg(VaList, node_ast *);
        }
        
        va_end(VaList);
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
#if 0
    printf("Creating Non Terminal Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal, Result->AstType);
    printf("NonTerminalType: %d ResultType: %d\n", NonTerminalType, Result->NonTerminal.Type);
    printf("ListType: %d ResultType: %d\n", ListType_empty, Result->ListType);
    printf("ChildCount: %d ResultCount: %d\n", ChildCount, Result->NonTerminal.ChildCount);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewIdentNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_ident;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
        Result->LineNumber = LineNumber;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewIntLitNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_int_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
        Result->LineNumber = LineNumber;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewStringLitNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_string_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
        Result->LineNumber = LineNumber;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

b32
CompareStrings(char *String1, char *String2)
{
    b32 Result = true;
#if 1
    char *A = String1;
    char *B = String2;
    
    while(*A != '\0' && *B != '\0')
    {
        if(*A++ != *B++)
        {
            Result = false;
            break;
        }
    }
    
    if(*A != *B)
    {
        Result = false;
    }
#endif
    return(Result);
}

void
AddVarToContext(node_ast *Context, char *VarName, node_ast *Type, b32 IsLocal)
{
    quack_var Var = {};
    Var.Name = VarName;
    Var.Type = Type;
    Var.IsLocal = IsLocal;
    Context->VarList[Context->VarCount++] = Var;
}

node_ast *
FindClassNode(char *Name, list<node_ast *> *ClassList)
{
    node_ast *Result = 0;
    
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *FirstChild = (*Iterator)->NonTerminal.Children[0];
            if(FirstChild)
            {
                char *ClassName = FirstChild->NonTerminal.Children[0]->Ident.Text;
                
                if(CompareStrings(ClassName, Name))
                {
                    Result = FirstChild;
                    break;
                }
            }
        }
    }
    
    return(Result);
}

void
ClearVisits(list<node_ast *> *ClassList)
{
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *Node = (*Iterator)->NonTerminal.Children[0];
            Node->Visited = false;
        }
    }
}

void
PrintError0(s32 IdentLineNumber, char *Text)
{
    if(ErrorCount < MaxErrorCount)
    {
        printf(Text, IdentLineNumber);
        ErrorCount++;
    }
}

void
PrintError1(s32 IdentLineNumber, char *Text, char *A)
{
    if(ErrorCount < MaxErrorCount)
    {
        printf(Text, IdentLineNumber, A);
        ErrorCount++;
    }
}

void
PrintError1d(s32 IdentLineNumber, char *Text, s32 A)
{
    if(ErrorCount < MaxErrorCount)
    {
        printf(Text, IdentLineNumber, A);
        ErrorCount++;
    }
}

void
PrintError2(s32 IdentLineNumber, char *Text, char *A, char *B)
{
    if(ErrorCount < MaxErrorCount)
    {
        printf(Text, IdentLineNumber, A, B);
        ErrorCount++;
    }
}

void
PrintError3(s32 IdentLineNumber, char *Text, char *A, char *B, char *C)
{
    if(ErrorCount < MaxErrorCount)
    {
        printf(Text, IdentLineNumber, A, B, C);
        ErrorCount++;
    }
}

void
WalkAst(node_ast *Node,  list<node_ast *> *ClassList, b32 *ConstructorsWellFormed)
{
    if(Node)
    {
#if 0
        printf("Walking Node\n");
        printf("AstType: %d \n", Node->AstType);
        printf("ListType: %d\n", Node->ListType);
        printf("\n--------------------\n");
#endif
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                if(Node->NonTerminal.Type == NonTerminalType_constructor)
                {
                    node_ast *ConstructorIdentNode = Node->NonTerminal.Children[0];
                    char *ConstructorName = ConstructorIdentNode->Ident.Text;
                    
                    if(!FindClassNode(ConstructorName, ClassList))
                    {
                        *ConstructorsWellFormed = false;
                    }
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        WalkAst(Node->NonTerminal.Children[Index], ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::const_iterator Iterator;
                for(Iterator = Node->List.List->begin();
                    Iterator != Node->List.List->end();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        WalkAst(*Iterator, ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
        }
    }
}

node_ast *
FindAstByClassName(node_ast *RootNode, char *ClassName)
{
    node_ast *Result = 0;
    node_ast *ClassListNode = RootNode->NonTerminal.Children[0];
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = ClassListNode->List.List->rbegin();
        Iterator != ClassListNode->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            if((*Iterator)->ClassName)
            {
                if(CompareStrings((*Iterator)->ClassName, ClassName))
                {
                    Result = (*Iterator);
                }
            }
        }
    }
    
    return(Result);
}

s32
FindCountClassName(node_ast *RootNode, char *ClassName)
{
    s32 Result = 0;
    node_ast *ClassListNode = RootNode->NonTerminal.Children[0];
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = ClassListNode->List.List->rbegin();
        Iterator != ClassListNode->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            if(CompareStrings((*Iterator)->ClassName, ClassName))
            {
                Result++;
            }
        }
    }
    
    return(Result);
}

b32
RequireSubtype(node_ast *Node, node_ast *Subnode)
{
    b32 Result = false;
    b32 NodesAreSame = CompareStrings(Node->ClassName, Subnode->ClassName);
    if(NodesAreSame)
    {
        Result = NodesAreSame;
    }
    else
    {
        if(Subnode->ClassExtend)
        {
            Result = RequireSubtype(Node, Subnode->ClassExtend);
        }
    }
    
    return(Result);
}

node_ast *
FindMethodInClass(node_ast *ClassNode, char *MethodName)
{
    node_ast *Result = 0;
    
    node_ast *ClassBody = ClassNode->NonTerminal.Children[1];
    node_ast *MethodList = ClassBody->NonTerminal.Children[1];
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = MethodList->List.List->rbegin();
        Iterator != MethodList->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *Node = (*Iterator);
            
            if(CompareStrings(Node->MethodName, MethodName))
            {
                Result = Node;
                break;
            }
        }
    }
    
    return(Result);
}

node_ast *
FindMethodInClassAndFollowParent(node_ast *ClassNode, char *MethodName)
{
    node_ast *Result = 0;
    node_ast *CurrentClass = ClassNode;
    
    for(;;)
    {
        if(!CurrentClass)
        {
            break;
        }
        
        node_ast *FoundClass = FindMethodInClass(CurrentClass, MethodName);
        if(FoundClass)
        {
            Result = FoundClass;
            break;
        }
        
        if(CompareStrings(CurrentClass->ClassName, (char *)"Obj"))
        {
            break;
        }
        
        CurrentClass = CurrentClass->ClassExtend;
    }
    
    return(Result);
}

s32
FindMethodCountInClass(node_ast *ClassNode, char *MethodName)
{
    s32 Result = 0;
    
    node_ast *ClassBody = ClassNode->NonTerminal.Children[1];
    node_ast *MethodList = ClassBody->NonTerminal.Children[1];
    
    list<node_ast *>::reverse_iterator Iterator;
    for(Iterator = MethodList->List.List->rbegin();
        Iterator != MethodList->List.List->rend();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *Node = (*Iterator);
            
            if(CompareStrings(Node->MethodName, MethodName))
            {
                Result++;
            }
        }
    }
    
    return(Result);
}

s32
IsInFullMethodList(char *MethodName, quack_full_method_list *FullList)
{
    s32 Result = -1;
    
    for(s32 Index = 0;
        Index < FullList->Count;
        ++Index)
    {
        if(CompareStrings(MethodName, FullList->List[Index].Name))
        {
            Result = Index;
            break;
        }
    }
    
    return(Result);
}

void
AddMethodToFullList(char *MethodName, node_ast *ClassContext, quack_full_method_list *FullList)
{
    quack_var NewMethod = {};
    NewMethod.Name = MethodName;
    NewMethod.Type = ClassContext;
    FullList->List[FullList->Count++] = NewMethod;
}

void
GetFullMethodList(node_ast *Node, quack_full_method_list *FullList)
{
    if(Node->ClassExtend)
    {
        GetFullMethodList(Node->ClassExtend, FullList);
    }
    
    node_ast *ClassContext = Node->ClassContext;
    if(ClassContext)
    {
        node_ast *ClassBody = ClassContext->NonTerminal.Children[1];
        node_ast *MethodList = ClassBody->NonTerminal.Children[1];
        
        if(MethodList)
        {
            list<node_ast *>::reverse_iterator Iterator;
            for(Iterator = MethodList->List.List->rbegin();
                Iterator != MethodList->List.List->rend();
                ++Iterator)
            {
                if(*Iterator)
                {
                    node_ast *Node = (*Iterator);
                    char *MethodName = Node->MethodName;
                    
                    s32 MethodIndexInFullList = IsInFullMethodList(MethodName, FullList);
                    if(MethodIndexInFullList >= 0)
                    {
                        FullList->List[MethodIndexInFullList].Name = MethodName;
                        FullList->List[MethodIndexInFullList].Type = ClassContext;
                    }
                    else
                    {
                        AddMethodToFullList(MethodName, ClassContext, FullList);
                    }
                }
            }
        }
    }
}

node_ast *
FindPreviousMethodImplementation(node_ast *ClassNode, node_ast *MethodNode)
{
    node_ast *Result = 0;
    
    node_ast *ClassExtend = ClassNode->ClassExtend;
    for(;;)
    {
        if(!ClassExtend)
        {
            break;
        }
        
        Result = FindMethodInClass(ClassExtend, MethodNode->MethodName);
        
        if(Result)
        {
            break;
        }
        
        ClassExtend = ClassExtend->ClassExtend;
    }
    
    return(Result);
}

b32
CheckAgainstUsedNamesInContext(node_ast *Context, char *Name, s32 LineNumber)
{
    b32 Result = false;
    
    for(s32 Index = 0;
        Index < Context->UsedNameCount;
        ++Index)
    {
        if(CompareStrings(Context->UsedNameList[Index].Name, Name))
        {
            Result = true;
            PrintError1(LineNumber, (char *)"%d: Illegal use of %s\n", 
                        Name);
            break;
        }
    }
    
    return(Result);
}

b32
CheckAgainstUsedNamesInContext_(node_ast *Context, char *Name)
{
    b32 Result = false;
    
    for(s32 Index = 0;
        Index < Context->UsedNameCount;
        ++Index)
    {
        if(CompareStrings(Context->UsedNameList[Index].Name, Name))
        {
            Result = true;
            break;
        }
    }
    
    return(Result);
}

void
AddNameToContext(node_ast *Context, char *Name)
{
    quack_name QuackName = {};
    
    if(!CheckAgainstUsedNamesInContext_(Context, Name))
    {
        Context->UsedNameList[Context->UsedNameCount++].Name = Name;
    }
}

void
CheckArgExtendAndReturnValidity(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_method:
                    {
                        node_ast *PrevMethodImplementation = 
                            FindPreviousMethodImplementation(Node->ClassContext, Node);
                        if(PrevMethodImplementation)
                        {
                            Node->OverridenFromClass = PrevMethodImplementation;
                            
                            if(PrevMethodImplementation->MethodArgCount == Node->MethodArgCount)
                            {
                                for(s32 Index = 0;
                                    Index < Node->MethodArgCount;
                                    ++Index)
                                {
                                    node_ast *ArgTypeNodeA = 
                                        FindAstByClassName(RootNode, Node->MethodArgList[Index].Type);
                                    node_ast *ArgTypeNodeB = 
                                        FindAstByClassName(RootNode, 
                                                           PrevMethodImplementation->MethodArgList[Index].Type);
                                    
                                    if(ArgTypeNodeA)
                                    {
                                        if(ArgTypeNodeB)
                                        {
                                            if(RequireSubtype(ArgTypeNodeB, ArgTypeNodeA))
                                            {
                                                
                                            }
                                            else
                                            {
                                                PrintError3(Node->LineNumber, 
                                                            (char *)"%d: %s of type %s must be a subtype of the corresponding argument of type %s\n", 
                                                            Node->MethodArgList[Index].Name,
                                                            Node->MethodArgList[Index].Type,
                                                            PrevMethodImplementation->MethodArgList[Index].Type);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PrintError1(Node->LineNumber, (char *)"%d: Argument type %s does not exist\n",
                                                    Node->MethodArgList[Index].Type);
                                    }
                                }
                            }
                            else
                            {
                                PrintError0(Node->LineNumber, 
                                            (char *)"%d: Number of arguments don't match\n");
                            }
                        }
                        
                        if(Node->MethodReturn)
                        {
                            if(FindAstByClassName(RootNode, Node->MethodReturn->ClassName))
                            {
                                
                            }
                            else
                            {
                                PrintError1(Node->LineNumber, (char *)"%d: %s being returned doesn't exist\n",
                                            Node->MethodReturn->ClassName);
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckArgExtendAndReturnValidity(Node->NonTerminal.Children[Index], RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CheckArgExtendAndReturnValidity(*Iterator, RootNode);
                    }
                }
            } break;
        }
    }
}

void
AddExtensionsAndReturns(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        node_ast *ClassSignatureNode = Node->NonTerminal.Children[0];
                        node_ast *ExtendIdentNode = ClassSignatureNode->NonTerminal.Children[2];
                        char *ExtendName = ExtendIdentNode->Ident.Text;
                        node_ast *FoundNode = FindAstByClassName(RootNode, ExtendName);
                        
                        if(FoundNode)
                        {
                            Node->ClassExtend = FoundNode;
                        }
                        else
                        {
                            if(!CompareStrings(ExtendName, (char *)""))
                            {
                                PrintError1(Node->LineNumber, (char *)"%d: Extention %s doesn't exist\n",
                                            ExtendName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        node_ast *ReturnIdentNode = Node->NonTerminal.Children[2];
                        char *ReturnType = ReturnIdentNode->Ident.Text;
                        if(ReturnType)
                        {
                            node_ast *FoundNode = FindAstByClassName(RootNode, ReturnType);
                            
                            if(FoundNode)
                            {
                                Node->MethodReturn = FoundNode;
                            }
                            else
                            {
                                PrintError1(Node->LineNumber, (char *)"%d: %s being returned doesn't exist\n",
                                            ReturnType);  
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        AddExtensionsAndReturns(Node->NonTerminal.Children[Index], RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        AddExtensionsAndReturns(*Iterator, RootNode);
                    }
                }
            } break;
        }
    }
}

node_ast *
LeastCommonAncestor(node_ast *L, node_ast *R)
{
    node_ast *Result = 0;
    node_ast *Left = L->ExpressionType;
    node_ast *Right = R->ExpressionType;
    
    b32 Searching = true;
    while(Searching)
    {
        for(;;)
        {
            if(CompareStrings(Left->ClassName,
                              Right->ClassName))
            {
                Result = Left;
                Searching = false;
                break;
            }
            else if(Left->ClassExtend)
            {
                Left = Left->ClassExtend;
            }
            else
            {
                break;
            }
        }
        
        if(Right->ClassExtend)
        {
            Right = Right->ClassExtend;
        }
        else
        {
            Searching = false;
        }
    }
    
    if(Result)
    {
        
    }
    else
    {
        
    }
    
    return(Result);
}

quack_var
FindVarInContext(node_ast *Context, char *VarName)
{
    quack_var Result = {};
    
    for(s32 Index = 0;
        Index < Context->VarCount;
        ++Index)
    {
        if(CompareStrings(VarName, Context->VarList[Index].Name))
        {
            Result = Context->VarList[Index];
            break;
        }
    }
    
    return(Result);
}

s32
FindVarIndexInContext(node_ast *Context, char *VarName)
{
    s32 Result = 0;
    for(s32 Index = 0;
        Index < Context->VarCount;
        ++Index)
    {
        if(CompareStrings(VarName, Context->VarList[Index].Name))
        {
            Result = Index;
            break;
        }
    }
    
    return(Result);
}

quack_var
FindVarInContextAndParentContexts(node_ast *Context, char *VarName)
{
    quack_var QuackVar = {};
    node_ast *CurrentContext = Context;
    node_ast *ExtendContext = 0;
    if(!Context->MethodContext)
    {
        ExtendContext = CurrentContext->ClassExtend;
    }
    
    for(;;)
    {
        if(CurrentContext)
        {
            if(!QuackVar.Type)
            {
                ExtendContext = CurrentContext->ClassExtend;
                QuackVar = FindVarInContext(CurrentContext, VarName);
                
                if(QuackVar.Type)
                {
                    break;
                }
                
                if(CurrentContext == GlobalContext)
                {
                    break;
                }
                
                if(!CurrentContext->MethodContext)
                {
                    if(CompareStrings(CurrentContext->ClassName, (char *)"Obj"))
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
                
                CurrentContext = ExtendContext;
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }
    
    return(QuackVar);
}

void
TypeInferExpression(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_plus:
                    case NonTerminalType_sub:
                    case NonTerminalType_mult:
                    case NonTerminalType_div:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        TypeInferExpression(Right, RootNode);
                        
                        if(Left->ExpressionType && Right->ExpressionType)
                        {
                            Node->ExpressionType = LeastCommonAncestor(Left, Right);
                            node_ast *IntNode = FindAstByClassName(RootNode, (char *)"Int");
                            if(Node->ExpressionType)
                            {
                                if(!RequireSubtype(IntNode, Node->ExpressionType))
                                {
                                    char *OperatorForType = (char *)"";
                                    switch(Node->NonTerminal.Type)
                                    {
                                        case NonTerminalType_plus: {OperatorForType = (char *)"+";} break;
                                        case NonTerminalType_sub: {OperatorForType = (char *)"-";} break;
                                        case NonTerminalType_mult: {OperatorForType = (char *)"*";} break;
                                        case NonTerminalType_div: {OperatorForType = (char *)"/";} break;
                                    }
                                    
                                    PrintError1(Left->LineNumber, (char *)"%d: %s operator requires subtype of int\n",
                                                OperatorForType);
                                }
                            }
                        }
                        else
                        {
                            if(!Left->ExpressionType)
                            {
                                
                            }
                        }
                    } break;
                    
                    case NonTerminalType_unary:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        TypeInferExpression(Left, RootNode);
                        
                        if(Left->ExpressionType)
                        {
                            Node->ExpressionType = Left->ExpressionType;
                            
                            node_ast *IntNode = FindAstByClassName(RootNode, (char *)"Int");
                            if(Node->ExpressionType)
                            {
                                if(!RequireSubtype(IntNode, Node->ExpressionType))
                                {
                                    PrintError0(Left->LineNumber, 
                                                (char *)"%d: Unary operator requires subtype of int\n");
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_not:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        
                        TypeInferExpression(Left, RootNode);
                        
                        if(Left->ExpressionType)
                        {
                            Node->ExpressionType = Left->ExpressionType;
                            
                            node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                            if(Node->ExpressionType)
                            {
                                if(!RequireSubtype(BooleanNode, Node->ExpressionType))
                                {
                                    PrintError0(Left->LineNumber, 
                                                (char *)"%d: NOT operator requires subtype of boolean\n");
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_and:
                    case NonTerminalType_or:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        TypeInferExpression(Right, RootNode);
                        
                        if(Left->ExpressionType && Right->ExpressionType)
                        {
                            Node->ExpressionType = LeastCommonAncestor(Left, Right);
                            
                            node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                            if(Node->ExpressionType)
                            {
                                if(!RequireSubtype(BooleanNode, Node->ExpressionType))
                                {
                                    char *OperatorForType = (char *)"";
                                    switch(Node->NonTerminal.Type)
                                    {
                                        case NonTerminalType_and: {OperatorForType = (char *)"AND";} break;
                                        case NonTerminalType_or: {OperatorForType = (char *)"OR";} break;
                                        case NonTerminalType_not: {OperatorForType = (char *)"NOT";} break;
                                    }
                                    
                                    PrintError1(Left->LineNumber, 
                                                (char *)"%d: %s operator requires subtype of boolean\n",
                                                OperatorForType);
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_equals:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        TypeInferExpression(Right, RootNode);
                        
                        if(Left->ExpressionType && Right->ExpressionType)
                        {
                            node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                            Node->ExpressionType = BooleanNode;
                        }
                    } break;
                    
                    case NonTerminalType_atmost:
                    case NonTerminalType_less:
                    case NonTerminalType_atleast:
                    case NonTerminalType_more:
                    {
                        node_ast *IntNode = FindAstByClassName(RootNode, (char *)"Int");
                        node_ast *StringNode = FindAstByClassName(RootNode, (char *)"String");
                        node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                        
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        TypeInferExpression(Right, RootNode);
                        
                        if(Left->ExpressionType && Right->ExpressionType)
                        {
                            Node->ExpressionType = LeastCommonAncestor(Left, Right);
                            
                            if(Node->ExpressionType)
                            {
                                if(RequireSubtype(IntNode, Node->ExpressionType))
                                {
                                    Node->ExpressionType = BooleanNode;
                                }
                                else if(RequireSubtype(StringNode, Node->ExpressionType))
                                {
                                    Node->ExpressionType = BooleanNode;
                                }
                                else
                                {
                                    char *OperatorForType = (char *)"";
                                    switch(Node->NonTerminal.Type)
                                    {
                                        case NonTerminalType_atmost: {OperatorForType = (char *)"<=";} break;
                                        case NonTerminalType_less: {OperatorForType = (char *)"<";} break;
                                        case NonTerminalType_atleast: {OperatorForType = (char *)">=";} break;
                                        case NonTerminalType_more: {OperatorForType = (char *)">";} break;
                                    }
                                    
                                    PrintError1(Left->LineNumber, 
                                                (char *)"%d: %s operator requires subtype of boolean or int\n",
                                                OperatorForType);
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_dot:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        
                        if(Left->ExpressionType)
                        {
                            char *AccessName = Right->Ident.Text;
                            
                            node_ast *AccessClassContext = 
                                FindAstByClassName(RootNode, Left->ExpressionType->ClassName);
                            if(AccessClassContext)
                            {
                                if(AccessName)
                                {
                                    quack_var Var = FindVarInContextAndParentContexts(AccessClassContext, AccessName);
                                    
                                    if(Var.Type)
                                    {
                                        Node->ExpressionType = Var.Type;
                                    }
                                    else
                                    {
                                        PrintError2(Left->LineNumber, (char *)"%d: Could not find %s in class %s\n",
                                                    AccessName, Left->ExpressionType->ClassName);
                                    }
                                }
                            }
                            else
                            {
                                PrintError1(Left->LineNumber, (char *)"%d: %s is not a class\n",
                                            Left->ExpressionType->ClassName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_constructor:
                    {
                        node_ast *IdentNode = Node->NonTerminal.Children[0];
                        char *IdentName = IdentNode->Ident.Text;
                        
                        if(IdentName)
                        {
                            
                            node_ast *ClassNode = FindAstByClassName(RootNode, IdentName);
                            if(ClassNode)
                            {
                                Node->ExpressionType = ClassNode;
                                
                                node_ast *ConstructorArgumentListNode = Node->NonTerminal.Children[1];
                                s32 Index = 0;
                                s32 ListLength = ConstructorArgumentListNode->List.List->size();
                                if(ListLength == ClassNode->ClassArgCount)
                                {
                                    list<node_ast *>::const_iterator Iterator;
                                    for(Iterator = ConstructorArgumentListNode->List.List->begin();
                                        Iterator != ConstructorArgumentListNode->List.List->end();
                                        ++Iterator)
                                    {
                                        if(*Iterator)
                                        {
                                            node_ast *ArgNode = (*Iterator);
                                            TypeInferExpression(ArgNode, RootNode);
                                            
                                            if(ArgNode->ExpressionType)
                                            {
                                                if(Index < ClassNode->ClassArgCount)
                                                {
                                                    quack_arg QuackArg = ClassNode->ClassArgList[Index];
                                                    node_ast *CorrespondingArgType = 
                                                        FindAstByClassName(RootNode, QuackArg.Type);
                                                    if(CorrespondingArgType)
                                                    {
                                                        if(RequireSubtype(CorrespondingArgType, 
                                                                          ArgNode->ExpressionType))
                                                        {
                                                            
                                                        }
                                                        else
                                                        {
                                                            PrintError1(Node->LineNumber, 
                                                                        (char *)"%d: Actual argument of type %s must be a subtype of the corresponding argument\n", ArgNode->ExpressionType->ClassName);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                PrintError1d(Node->LineNumber, 
                                                             (char *)"%d: Could not find argument #d\n",
                                                             (Index+1));
                                            }
                                            
                                            ++Index;
                                        }
                                    }
                                }
                                else
                                {
                                    PrintError1(Node->LineNumber, 
                                                (char *)"%d: Number of arguments for constructor %s do not match number arguments required for the class\n", IdentName);
                                }
                            }
                            else
                            {
                                PrintError1(Node->LineNumber, 
                                            (char *)"%d: %s is not a class\n", IdentName);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_accessor:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        TypeInferExpression(Left, RootNode);
                        
                        if(Left->ExpressionType)
                        {
                            char *AccessName = Right->Ident.Text;
                            
                            node_ast *AccessClassContext = 
                                FindAstByClassName(RootNode, Left->ExpressionType->ClassName);
                            if(AccessClassContext)
                            {
                                node_ast *AccessMethod = FindMethodInClassAndFollowParent(AccessClassContext, AccessName);
                                
                                if(AccessMethod)
                                {
                                    if(AccessMethod->MethodReturn)
                                    {
                                        Node->ExpressionType = AccessMethod->MethodReturn;
                                    }
                                    
                                    node_ast *AccessArgumentListNode = Node->NonTerminal.Children[2];
                                    s32 Index = 0;
                                    s32 ListLength = AccessArgumentListNode->List.List->size();
                                    if(ListLength == AccessMethod->MethodArgCount)
                                    {
                                        list<node_ast *>::const_iterator Iterator;
                                        for(Iterator = AccessArgumentListNode->List.List->begin();
                                            Iterator != AccessArgumentListNode->List.List->end();
                                            ++Iterator)
                                        {
                                            if(*Iterator)
                                            {
                                                node_ast *ArgNode = (*Iterator);
                                                TypeInferExpression(ArgNode, RootNode);
                                                
                                                if(ArgNode->ExpressionType)
                                                {
                                                    if(Index < AccessMethod->MethodArgCount)
                                                    {
                                                        quack_arg QuackArg = AccessMethod->MethodArgList[Index];
                                                        node_ast *CorrespondingArgType = 
                                                            FindAstByClassName(RootNode, QuackArg.Type);
                                                        
                                                        if(CorrespondingArgType)
                                                        {
                                                            if(RequireSubtype(CorrespondingArgType, 
                                                                              ArgNode->ExpressionType))
                                                            {
                                                                
                                                            }
                                                            else
                                                            {
                                                                PrintError1(Left->LineNumber, 
                                                                            (char *)"Actual argument of type %s must be a subtype of the corresponding argument",
                                                                            ArgNode->ExpressionType->ClassName);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    PrintError1d(Left->LineNumber, 
                                                                 (char *)"%d: Could not find argument #d\n", 
                                                                 (Index+1));
                                                }
                                                
                                                ++Index;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PrintError1(Left->LineNumber, (char *)"%d: Number of arguments for method %s do not match the number of arguments required for the method\n", AccessName);
                                    }
                                }
                                else
                                {
                                    PrintError2(Left->LineNumber, (char *)"%d: %s is not a method in class %s\n", 
                                                AccessName, Left->ExpressionType->ClassName);
                                }
                            }
                            else
                            {
                                PrintError1(Left->LineNumber, 
                                            (char *)"%d: %s is not a class\n", Left->ExpressionType->ClassName);
                            }
                        }
                    } break;
                }
                
            } break;
            
            case AstType_int_lit:
            {
                Node->ExpressionType = FindAstByClassName(RootNode, (char *)"Int");
            } break;
            
            case AstType_string_lit:
            {
                Node->ExpressionType = FindAstByClassName(RootNode, (char *)"String");
            } break;
            
            case AstType_ident:
            {
                node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                node_ast *NothingNode = FindAstByClassName(RootNode, (char *)"Nothing");
                b32 StopChecking = false;
                if(CompareStrings(Node->Ident.Text, (char *)"true") ||
                   CompareStrings(Node->Ident.Text, (char *)"false") || 
                   CompareStrings(Node->Ident.Text, (char *)"not"))
                {
                    Node->ExpressionType = BooleanNode;
                    StopChecking = true;
                }
                else if(CompareStrings(Node->Ident.Text, (char *)"Nothing"))
                {
                    Node->ExpressionType = NothingNode;
                    StopChecking = true;
                }
                else if(Node->ClassContext)
                {
                    if(CompareStrings(Node->Ident.Text, (char *)"this"))
                    {
                        Node->ExpressionType = Node->ClassContext;
                        StopChecking = true;
                    }
                }
                
                if(!StopChecking)
                {
                    node_ast *CurrentContext = GlobalContext;
                    if(Node->ClassContext)
                    {
                        if(Node->MethodContext)
                        {
                            CurrentContext = Node->MethodContext;
                        }
                        else
                        {
                            CurrentContext = Node->ClassContext;
                        }
                    }
                    
                    
                    b32 IsFound = false;
                    quack_var QuackVar = 
                        FindVarInContextAndParentContexts(CurrentContext, Node->Ident.Text);
                    if(QuackVar.Type)
                    {
                        Node->ExpressionType = QuackVar.Type;
                        IsFound = true;
                    }
                    else
                    {
                        if(Node->ClassContext)
                        {
                            for(s32 Index = 0;
                                Index < Node->ClassContext->ClassArgCount;
                                ++Index)
                            {
                                quack_arg QuackArg = Node->ClassContext->ClassArgList[Index];
                                if(QuackArg.Type)
                                {
                                    node_ast *ArgType = FindAstByClassName(RootNode, QuackArg.Type);
                                    if(ArgType)
                                    {
                                        Node->ExpressionType = ArgType;
                                        IsFound = true;
                                    }
                                }
                            }
                        }
                        
                        if(!IsFound)
                        {
                            if(Node->MethodContext)
                            {
                                for(s32 Index = 0;
                                    Index < Node->MethodContext->MethodArgCount;
                                    ++Index)
                                {
                                    quack_arg QuackArg = Node->MethodContext->MethodArgList[Index];
                                    if(QuackArg.Type)
                                    {
                                        node_ast *ArgType = FindAstByClassName(RootNode, QuackArg.Type);
                                        if(ArgType)
                                        {
                                            Node->ExpressionType = ArgType;
                                            IsFound = true;
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    if(!IsFound)
                    {
                        if(CurrentContext->MethodContext)
                        {
                            PrintError2(Node->LineNumber, (char *)"%d: %s could not be found in %s\n",
                                        Node->Ident.Text, CurrentContext->MethodName);
                        }
                        else
                        {
                            char *ContextName = 0;
                            if(CurrentContext->ClassName)
                            {
                                ContextName = CurrentContext->ClassName;
                            }
                            
                            if(CurrentContext == GlobalContext)
                            {
                                ContextName = (char *)"Global";
                            }
                            PrintError2(Node->LineNumber, (char *)"%d: %s could not be found in %s\n",
                                        Node->Ident.Text, ContextName);
                        }
                    }
                }
            } break;
        }
    }
}

void
AddInstanceVarToContext(node_ast *Context, char *VarName)
{
    quack_name Var = {};
    Var.Name = VarName;
    Context->InstanceVarList[Context->InstanceVarCount++] = Var;
}

void
BuildVarContext(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_gets:
                    {
                        node_ast *LeftNode = Node->NonTerminal.Children[0];
                        node_ast *OptionalTypeNode = Node->NonTerminal.Children[1];
                        node_ast *RightNode = Node->NonTerminal.Children[2];
                        
                        TypeInferExpression(RightNode, RootNode);
                        char *VarName = 0;
                        
                        if(RightNode->ExpressionType)
                        {
                            node_ast *VarContext = 0;
                            b32 IsLocalVar = false;
                            
                            node_ast *ExpressionType = 0;
                            if(LeftNode->NonTerminal.ChildCount == 2)
                            {
                                node_ast *RExpressionNode = LeftNode->NonTerminal.Children[0];
                                TypeInferExpression(RExpressionNode, RootNode);
                                
                                if(RExpressionNode->ExpressionType)
                                {
                                    ExpressionType = RExpressionNode->ExpressionType;
                                    node_ast *VarNode = LeftNode->NonTerminal.Children[1];
                                    VarName = VarNode->Ident.Text;
                                    
                                }
                            }
                            
                            if(LeftNode->AstType == AstType_ident)
                            {
                                IsLocalVar = true;
                                if(Node->MethodContext)
                                {
                                    ExpressionType = Node->MethodContext;
                                    VarName = LeftNode->Ident.Text;
                                }
                                else if(Node->ClassContext)
                                {
                                    ExpressionType = Node->ClassContext;
                                    VarName = LeftNode->Ident.Text;
                                }
                                else
                                {
                                    ExpressionType = GlobalContext;
                                    VarName = LeftNode->Ident.Text;
                                    
                                }
                            }
                            
                            if(VarName)
                            {
                                if(ExpressionType)
                                {
                                    node_ast *CurrentContext = GlobalContext;
                                    if(Node->MethodContext)
                                    {
                                        CurrentContext = Node->MethodContext;
                                    }
                                    else if(Node->ClassContext)
                                    {
                                        CurrentContext = Node->ClassContext;
                                    }
                                    
                                    //if(CompareStrings(ExpressionType->ClassName, CurrentContext->ClassName))
                                    //{
                                    quack_var Var = {};
                                    if(Node->MethodContext)
                                    {
                                        Var = FindVarInContext(ExpressionType, 
                                                               VarName);
                                    }
                                    else if(Node->ClassContext)
                                    {
                                        if(IsLocalVar)
                                        {
                                            Var = FindVarInContext(ExpressionType, 
                                                                   VarName);
                                        }
                                        else
                                        {
                                            Var = FindVarInContextAndParentContexts(ExpressionType, 
                                                                                    VarName);
                                        }
                                    }
                                    else
                                    {
                                        Var = FindVarInContext(ExpressionType, 
                                                               VarName);
                                    }
                                    
                                    if(Var.Type)
                                    {
                                        if(IsLocalVar)
                                        {
                                            if(Var.IsLocal)
                                            {
                                                s32 VarIndex = 0;
                                                if(Node->MethodContext)
                                                {
                                                    VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                     VarName);
                                                }
                                                else if(Node->ClassContext)
                                                {
                                                    VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                     VarName);
                                                }
                                                else
                                                {
                                                    VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                     VarName);
                                                }
                                                
                                                node_ast *Optional = 0;
                                                if(OptionalTypeNode)
                                                {
                                                    Optional = FindAstByClassName(RootNode, 
                                                                                  OptionalTypeNode->Ident.Text);
                                                    if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                       (char *)"Nothing"))
                                                    {
                                                        if(Optional)
                                                        {
                                                            if(!RequireSubtype(Optional, RightNode->ExpressionType))
                                                            {
                                                                Optional = 0;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                                if(RequireSubtype(Var.Type, RightNode->ExpressionType) && Optional)
                                                {
                                                    if(!CompareStrings(Var.Type->ClassName,
                                                                       RightNode->ExpressionType->ClassName))
                                                    {
                                                        VarBubbling = true;
                                                    }
                                                    if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                       (char *)"Nothing"))
                                                    {
                                                        Node->ExpressionType = Optional;
                                                        ExpressionType->VarList[VarIndex].Type = Optional;
                                                        
                                                    }
                                                    else
                                                    {
                                                        Node->ExpressionType = RightNode->ExpressionType;
                                                        ExpressionType->VarList[VarIndex].Type = RightNode->ExpressionType;
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    if(VarBubblingDone)
                                                    {
                                                        PrintError2(Node->LineNumber, 
                                                                    (char *)"%d: Reassignment of variable %s requires a supertype of its type %s\n", 
                                                                    Var.Name, Var.Type->ClassName);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                PrintError0(Node->LineNumber, 
                                                            (char *)"%d: Cannot assign a local variable with the same name as an instance variable\n");
                                            }
                                        }
                                        else
                                        {
                                            if(!Node->MethodContext)
                                            {
                                                if(Node->ClassContext)
                                                {
                                                    if(!CompareStrings(Node->ClassContext->ClassName, 
                                                                       (char *)"Global"))
                                                    {
                                                        s32 VarIndex = 0;
                                                        if(Node->MethodContext)
                                                        {
                                                            VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                             VarName);
                                                        }
                                                        else if(Node->ClassContext)
                                                        {
                                                            VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                             VarName);
                                                        }
                                                        else
                                                        {
                                                            VarIndex = FindVarIndexInContext(ExpressionType, 
                                                                                             VarName);
                                                        }
                                                        
                                                        node_ast *Optional = 0;
                                                        if(OptionalTypeNode)
                                                        {
                                                            Optional = FindAstByClassName(RootNode, 
                                                                                          OptionalTypeNode->Ident.Text);
                                                            
                                                            if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                               (char *)"Nothing"))
                                                            {
                                                                if(Optional)
                                                                {
                                                                    if(!RequireSubtype(Optional, RightNode->ExpressionType))
                                                                    {
                                                                        Optional = 0;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                        if(Optional)
                                                        {
                                                            if(RequireSubtype(RightNode->ExpressionType, Var.Type))
                                                            {
                                                                if(!CompareStrings(Var.Type->ClassName,
                                                                                   RightNode->ExpressionType->ClassName))
                                                                {
                                                                    VarBubbling = true;
                                                                }
                                                                
                                                                if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                                   (char *)"Nothing"))
                                                                {
                                                                    if(VarBubblingDone)
                                                                    {
                                                                        PrintError0(Node->LineNumber,
                                                                                    (char *)"%d: Optional type must by a supertype of the assignment type\n");
                                                                    }
                                                                    Node->ExpressionType = Optional;
                                                                    ExpressionType->VarList[VarIndex].Type = Optional;
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    Node->ExpressionType = RightNode->ExpressionType;
                                                                    ExpressionType->VarList[VarIndex].Type = RightNode->ExpressionType;
                                                                    
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if(VarBubblingDone)
                                                            {
                                                                PrintError0(Node->LineNumber, 
                                                                            (char *)"%d: Optional type does not exist\n");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        PrintError0(Node->LineNumber, 
                                                                    (char *)"%d: Can't assign instance variables in the global scope\n");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                PrintError0(Node->LineNumber, 
                                                            (char *)"%d: Can't assign instance variables outside of the class they were created in\n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(IsLocalVar)
                                        {
                                            node_ast *Context = GlobalContext;
                                            if(Node->ClassContext)
                                            {
                                                Context = Node->ClassContext;
                                            }
                                            CheckAgainstUsedNamesInContext(Context, 
                                                                           VarName, Node->LineNumber);
                                            char *ContextName = 0;
                                            if(ExpressionType->MethodContext)
                                            {
                                                ContextName = ExpressionType->MethodName;
                                            }
                                            else
                                            {
                                                ContextName = ExpressionType->ClassName;
                                            }
                                            
                                            if(Context == GlobalContext)
                                            {
                                                ContextName = (char *)"Global";
                                            }
                                            
                                            if(OptionalTypeNode)
                                            {
                                                if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                   (char *)"Nothing"))
                                                {
                                                    node_ast *OptionalNode = 
                                                        FindAstByClassName(RootNode, OptionalTypeNode->Ident.Text);
                                                    if(OptionalNode)
                                                    {
                                                        if(RequireSubtype(OptionalNode, RightNode->ExpressionType))
                                                        {
                                                            AddVarToContext(ExpressionType, VarName, 
                                                                            OptionalNode, true);
                                                        }
                                                        else
                                                        {
                                                            if(VarBubblingDone)
                                                            {
                                                                PrintError0(Node->LineNumber,
                                                                            (char *)"%d: Optional type must be a supertype of the assignment type\n");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        PrintError1(Node->LineNumber, 
                                                                    (char *)"%d: Optional type %s does not exist\n",
                                                                    OptionalTypeNode->Ident.Text);
                                                    }
                                                }
                                                else
                                                {
                                                    AddVarToContext(ExpressionType, VarName, 
                                                                    RightNode->ExpressionType, true);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if(!Node->MethodContext)
                                            {
                                                if(Node->ClassContext)
                                                {
                                                    if(!CompareStrings(Node->ClassContext->ClassName, 
                                                                       (char *)"Global"))
                                                    {
                                                        if(CompareStrings(Node->ClassContext->ClassName,
                                                                          ExpressionType->ClassName))
                                                        {
                                                            if(OptionalTypeNode)
                                                            {
                                                                if(!CompareStrings(OptionalTypeNode->Ident.Text,
                                                                                   (char *)"Nothing"))
                                                                {
                                                                    node_ast *OptionalNode = 
                                                                        FindAstByClassName(RootNode, OptionalTypeNode->Ident.Text);
                                                                    if(OptionalNode)
                                                                    {
                                                                        if(RequireSubtype(OptionalNode, RightNode->ExpressionType))
                                                                        {
                                                                            CheckAgainstUsedNamesInContext(Node->ClassContext, 
                                                                                                           VarName, Node->LineNumber);
                                                                            AddVarToContext(ExpressionType, VarName, 
                                                                                            OptionalNode, false);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        PrintError1(Node->LineNumber, 
                                                                                    (char *)"%d: Optional type %s does not exist\n",
                                                                                    OptionalTypeNode->Ident.Text);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    CheckAgainstUsedNamesInContext(Node->ClassContext, 
                                                                                                   VarName, Node->LineNumber);
                                                                    AddVarToContext(ExpressionType, VarName, 
                                                                                    RightNode->ExpressionType, false);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            PrintError1(Node->LineNumber, 
                                                                        (char *)"%d: Can't create instance variable %s from a different class\n", 
                                                                        VarName);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        PrintError0(Node->LineNumber,
                                                                    (char *)"%d: Can't create instance variables in global scope\n");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                PrintError0(Node->LineNumber, 
                                                            (char *)"%d: Can't create instance variables in method scope\n");
                                            }
                                        }
                                        if(!Node->MethodContext)
                                        {
                                            //if(!CheckNameAgainstMethodArgs())
                                            //{
                                            //AddVarToContext(ExpressionType, VarName, 
                                            //RightNode->ExpressionType);
                                            //}
                                        }
                                        else if(Node->ClassContext)
                                        {
                                            //if(!CheckNameAgainstClassArgs())
                                            //{
                                            //AddVarToContext(ExpressionType, VarName, 
                                            //RightNode->ExpressionType);
                                            //}
                                        }
                                        else
                                        {
                                            //AddVarToContext(ExpressionType, VarName, 
                                            //RightNode->ExpressionType);
                                        }
                                    }
                                    //}
                                    //else
                                    //{
                                    //printf("ERROR    :    Can't \n", VarName);
                                    // Error here
                                    //}
                                }
                                else
                                {
                                    PrintError1(Node->LineNumber, 
                                                (char *)"%d: Expression to the left of %s is not valid\n",
                                                VarName);
                                }
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        BuildVarContext(Node->NonTerminal.Children[Index], RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        BuildVarContext((*Iterator), RootNode);
                    }
                }
            } break;
        }
    }
}

void
TypeInferAllExpressions(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_if:
                    {
                        node_ast *RExpressionNode = Node->NonTerminal.Children[0];
                        TypeInferExpression(RExpressionNode, RootNode);
                        node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                        
                        if(RExpressionNode->ExpressionType)
                        {
                            if(!RequireSubtype(BooleanNode, RExpressionNode->ExpressionType))
                            {
                                PrintError0(RExpressionNode->LineNumber, 
                                            (char *)"%d: Argument in \"if\" statement must be of subtype boolean\n");
                            }
                        }
                        
                        node_ast *ElifListNode = Node->NonTerminal.Children[2];
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ElifListNode->List.List->begin();
                            Iterator != ElifListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *RExpNode = (*Iterator);
                                
                                ++Iterator;
                                if(*Iterator)
                                {
                                    node_ast *StatementBlockNode = (*Iterator);
                                    
                                    TypeInferExpression(RExpNode, RootNode);
                                    node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                                    
                                    if(RExpNode->ExpressionType)
                                    {
                                        if(!RequireSubtype(BooleanNode, RExpNode->ExpressionType))
                                        {
                                            PrintError0(RExpressionNode->LineNumber, 
                                                        (char *)"%d: Argument in \"if\" statement must be of subtype boolean\n");
                                        }
                                    }
                                }
                            }
                        }
                        
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        node_ast *RExpressionNode = Node->NonTerminal.Children[0];
                        TypeInferExpression(RExpressionNode, RootNode);
                        node_ast *BooleanNode = FindAstByClassName(RootNode, (char *)"Boolean");
                        
                        if(RExpressionNode->ExpressionType)
                        {
                            if(!RequireSubtype(BooleanNode, RExpressionNode->ExpressionType))
                            {
                                PrintError0(RExpressionNode->LineNumber, 
                                            (char *)"%d: Argument in \"while\" statement must be of subtype boolean\n");
                            }
                        }
                    } break;
                    
                    case NonTerminalType_return:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        if(NextNode)
                        {
                            TypeInferExpression(NextNode, RootNode);
                            
                            if(NextNode->ExpressionType)
                            {
                                if(NextNode->MethodContext && NextNode->MethodContext->MethodReturn)
                                {
                                    if(!RequireSubtype(NextNode->MethodContext->MethodReturn, 
                                                       NextNode->ExpressionType))
                                    {
                                        PrintError3(NextNode->LineNumber,
                                                    (char *)"%d: Type returned %s must be a subtype of %s, the type returned by the method %s\n",
                                                    NextNode->ExpressionType->ClassName,
                                                    NextNode->MethodContext->MethodReturn->ClassName,
                                                    NextNode->MethodContext->MethodName);
                                    }
                                }
                                else
                                {
                                    PrintError0(NextNode->LineNumber,
                                                (char *)"%d: Return statements must only be inside methods\n");
                                }
                            }
                        }
                    } break;
                    
                    case NonTerminalType_rexpression:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        TypeInferExpression(NextNode, RootNode);
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        TypeInferAllExpressions(Node->NonTerminal.Children[Index], RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        TypeInferAllExpressions((*Iterator), RootNode);
                    }
                }
            } break;
        }
    }
}

void
CheckClassAndMethodArgNameClash(node_ast *Node, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class_signature:
                    {
                        node_ast *ClassArgumentListNode = Node->NonTerminal.Children[1];
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ClassArgumentListNode->List.List->begin();
                            Iterator != ClassArgumentListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *TypeNode = *Iterator;
                                node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
                                char *Name = NameIdentNode->Ident.Text;
                                CheckAgainstUsedNamesInContext(GlobalContext, Name, TypeNode->LineNumber);
                            }
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        node_ast *MethodIdentNode = Node->NonTerminal.Children[0];
                        char *MethodName = MethodIdentNode->Ident.Text;
                        CheckAgainstUsedNamesInContext(GlobalContext, MethodName, Node->LineNumber);
                        
                        node_ast *MethodArgumentListNode = Node->NonTerminal.Children[1];
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = MethodArgumentListNode->List.List->begin();
                            Iterator != MethodArgumentListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *NameIdentNode = (*Iterator)->NonTerminal.Children[0];
                                char *Name = NameIdentNode->Ident.Text;
                                CheckAgainstUsedNamesInContext(GlobalContext, Name, NameIdentNode->LineNumber);
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckClassAndMethodArgNameClash(Node->NonTerminal.Children[Index], RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CheckClassAndMethodArgNameClash((*Iterator), RootNode);
                    }
                }
            } break;
        }
    }
}

void
CreateInstanceVarList(node_ast *Node)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_if:
                    {
                        node_ast *StatementBlockNode = Node->NonTerminal.Children[1];
                        CreateInstanceVarList(StatementBlockNode);
                        
                        node_ast *ElifListNode = Node->NonTerminal.Children[2];
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ElifListNode->List.List->begin();
                            Iterator != ElifListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ElifStatementBlockNode = (*Iterator)->NonTerminal.Children[1];
                                CreateInstanceVarList(ElifStatementBlockNode);
                            }
                        }
                        
                        node_ast *ElseStatementBlockNode = Node->NonTerminal.Children[3];
                        CreateInstanceVarList(ElseStatementBlockNode);
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        node_ast *WhileStatementBlockNode = Node->NonTerminal.Children[1];
                        CreateInstanceVarList(WhileStatementBlockNode);
                    } break;
                    
                    case NonTerminalType_gets:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Optional = Node->NonTerminal.Children[1];
                        node_ast *Right = Node->NonTerminal.Children[2];
                        
                        CreateInstanceVarList(Left);
                    } break;
                    
                    case NonTerminalType_dot:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        if(Left->AstType == AstType_ident && 
                           Right->AstType == AstType_ident)
                        {
                            char *LeftIdent = Left->Ident.Text;
                            char *RightIdent = Right->Ident.Text;
                            
                            if(CompareStrings(LeftIdent, (char *)"this"))
                            {
                                AddInstanceVarToContext(Node->ClassContext, RightIdent);
                            }
                        }
                    } break;
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CreateInstanceVarList((*Iterator));
                    }
                }
            } break;
        }
    }
}

void
CheckAllInstanceVarInit(node_ast *Node)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        if(Node->ClassExtend)
                        {
                            for(s32 ExtendIndex = 0;
                                ExtendIndex < Node->ClassExtend->InstanceVarCount;
                                ++ExtendIndex)
                            {
                                b32 VarExists = false;
                                quack_name ExtendVar = Node->ClassExtend->InstanceVarList[ExtendIndex];
                                
                                for(s32 Index = 0;
                                    Index < Node->ClassContext->InstanceVarCount;
                                    ++Index)
                                {
                                    quack_name Var = Node->ClassContext->InstanceVarList[Index];
                                    
                                    if(CompareStrings(Var.Name, ExtendVar.Name))
                                    {
                                        VarExists = true;
                                    }
                                }
                                
                                if(!VarExists)
                                {
                                    PrintError2(Node->LineNumber,
                                                (char *)"%d: Did not initialize instance variable %s in class %s\n",
                                                ExtendVar.Name, Node->ClassContext->ClassName);
                                }
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckAllInstanceVarInit(Node->NonTerminal.Children[Index]);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CheckAllInstanceVarInit((*Iterator));
                    }
                }
            } break;
        }
    }
}

void 
BuildInstanceVarContext(node_ast *Node)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class_body:
                    {
                        node_ast *StatementListNode = Node->NonTerminal.Children[0];
                        CreateInstanceVarList(StatementListNode);
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        BuildInstanceVarContext(Node->NonTerminal.Children[Index]);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        BuildInstanceVarContext((*Iterator));
                    }
                }
            } break;
        }
    }
}

b32 
CheckReturnFlowExecution(node_ast *Node)
{
    b32 Result = false;
    
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckReturnFlowExecution(Node->NonTerminal.Children[Index]);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                if(Node->ListType == ListType_statement)
                {
                    list<node_ast *>::const_iterator Iterator;
                    for(Iterator = Node->List.List->begin();
                        Iterator != Node->List.List->end();
                        ++Iterator)
                    {
                        if(*Iterator)
                        {
                            node_ast *StatementNode = (*Iterator);
                            switch(StatementNode->NonTerminal.Type)
                            {
                                case NonTerminalType_return:
                                {
                                    Result = true;
                                } break;
                                
                                case NonTerminalType_if:
                                {
                                    b32 IfResult = false;
                                    b32 ElifResult = true;
                                    b32 ElseResult = false;
                                    node_ast *IfNode = (*Iterator);
                                    node_ast *OptionalElse = IfNode->NonTerminal.Children[3];
                                    
                                    if(OptionalElse->ListType != ListType_empty)
                                    {
                                        node_ast *StatementBlockNode = IfNode->NonTerminal.Children[1];
                                        node_ast *ElifListNode = IfNode->NonTerminal.Children[2];
                                        node_ast *ElseStatementBlockNode = IfNode->NonTerminal.Children[3];
                                        
                                        IfResult = CheckReturnFlowExecution(StatementBlockNode);
                                        
                                        list<node_ast *>::const_iterator ElifIterator;
                                        for(ElifIterator = ElifListNode->List.List->begin();
                                            ElifIterator != ElifListNode->List.List->end();
                                            ++ElifIterator)
                                        {
                                            if(*ElifIterator)
                                            {
                                                node_ast *ElifStatementBlockNode = (*ElifIterator)->NonTerminal.Children[1];
                                                ElifResult &= CheckReturnFlowExecution(ElifStatementBlockNode);
                                            }
                                        }
                                        
                                        ElseResult = CheckReturnFlowExecution(ElseStatementBlockNode);
                                        
                                        b32 TotalIfResult = IfResult & ElifResult & ElseResult;
                                        Result |= TotalIfResult;
                                    }
                                } break;
                            }
                        }
                    }
                }
                else
                {
                    list<node_ast *>::reverse_iterator Iterator;
                    for(Iterator = Node->List.List->rbegin();
                        Iterator != Node->List.List->rend();
                        ++Iterator)
                    {
                        if(*Iterator)
                        {
                            CheckReturnFlowExecution((*Iterator));
                        }
                    }
                }
            } break;
        }
    }
    
    return(Result);
}

void 
CheckReturnFlowExecutionAllMethods(node_ast *Node)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_method:
                    {
                        if(!(CompareStrings(Node->ClassContext->ClassName, (char *)"Obj") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"String") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Boolean") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Int") || 
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Nothing")))
                        {
                            node_ast *StatementListNode = Node->NonTerminal.Children[3];
                            b32 Result = CheckReturnFlowExecution(StatementListNode);
                            
                            if(!Result)
                            {
                                PrintError1(Node->LineNumber, 
                                            (char *)"%d: Method %s does not have a return on all execution paths\n",
                                            Node->MethodName);
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckReturnFlowExecutionAllMethods(Node->NonTerminal.Children[Index]);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CheckReturnFlowExecutionAllMethods((*Iterator));
                    }
                }
            } break;
        }
    }
}

void
BuildContextAST(node_ast *Node, node_ast *CurrentClassContext, node_ast *CurrentMethodContext, 
                node_ast *RootNode)
{
    node_ast *ClassContext = CurrentClassContext;
    node_ast *MethodContext = CurrentMethodContext;
    
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        node_ast *ClassSignatureNode = Node->NonTerminal.Children[0];
                        node_ast *ClassIdentNode = ClassSignatureNode->NonTerminal.Children[0];
                        char *ClassName = ClassIdentNode->Ident.Text;
                        ClassContext = Node;
                        ClassContext->ClassName = ClassName;
                        ClassContext->ClassArgCount = 0;
                        
                        
                        AddNameToContext(ClassContext, ClassName);
                        
                        CheckAgainstUsedNamesInContext(GlobalContext, ClassName, Node->LineNumber);
                        AddNameToContext(GlobalContext, ClassName);
                    } break;
                    
                    case NonTerminalType_class_signature:
                    {
                        node_ast *ClassArgumentListNode = Node->NonTerminal.Children[1];
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ClassArgumentListNode->List.List->begin();
                            Iterator != ClassArgumentListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *TypeNode = *Iterator;
                                node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
                                node_ast *TypeIdentNode = TypeNode->NonTerminal.Children[1];
                                
                                char *Name = NameIdentNode->Ident.Text;
                                char *Type = TypeIdentNode->Ident.Text;
                                
                                CheckAgainstUsedNamesInContext(ClassContext, Name, TypeNode->LineNumber);
                                //AddNameToContext(ClassContext, Name);
                                
                                ClassContext->ClassArgList[Index].Name = Name;
                                ClassContext->ClassArgList[Index].Type = Type;
                                
                                ClassContext->ClassArgCount++;
                                ++Index;
                            }
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        node_ast *MethodIdentNode = Node->NonTerminal.Children[0];
                        char *MethodName = MethodIdentNode->Ident.Text;
                        MethodContext = Node;
                        MethodContext->MethodName = MethodName;
                        MethodContext->MethodArgCount = 0;
                        MethodContext->OverridenFromClass = 0;
                        
                        CheckAgainstUsedNamesInContext(ClassContext, MethodName, Node->LineNumber);
                        AddNameToContext(ClassContext, MethodName);
                        AddNameToContext(MethodContext, MethodName);
                        
                        node_ast *MethodArgumentListNode = Node->NonTerminal.Children[1];
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = MethodArgumentListNode->List.List->begin();
                            Iterator != MethodArgumentListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *TypeNode = *Iterator;
                                node_ast *NameIdentNode = TypeNode->NonTerminal.Children[0];
                                node_ast *TypeIdentNode = TypeNode->NonTerminal.Children[1];
                                
                                char *Name = NameIdentNode->Ident.Text;
                                char *Type = TypeIdentNode->Ident.Text;
                                
                                CheckAgainstUsedNamesInContext(MethodContext, Name, TypeNode->LineNumber);
                                //AddNameToContext(MethodContext, Name);
                                
                                MethodContext->MethodArgList[Index].Name = Name;
                                MethodContext->MethodArgList[Index].Type = Type;
                                
                                MethodContext->MethodArgCount++;
                                ++Index;
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        BuildContextAST(Node->NonTerminal.Children[Index], 
                                        ClassContext, MethodContext, RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        BuildContextAST(*Iterator, ClassContext, MethodContext, RootNode);
                    }
                }
            } break;
        }
        
        Node->ClassContext = ClassContext;
        Node->MethodContext = MethodContext;
    }
}

void
CheckWalk(node_ast *Node)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        if(Node->ClassName)
                        {
#if 0
                            printf("A class: %s \n", Node->ClassContext->ClassName);
                            
                            for(s32 Index = 0;
                                Index < Node->ClassArgCount;
                                ++Index)
                            {
                                
                            }
#endif
                            if(Node->ClassExtend)
                            {
                                
                            }
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
#if 0
                        printf("Method %s in Class %s\n", Node->MethodContext->MethodName, 
                               Node->ClassContext->ClassName);
                        
                        for(s32 Index = 0;
                            Index < Node->MethodArgCount;
                            ++Index)
                        {
                            printf("METHOD Argument Name %s Type %s\n", 
                                   Node->MethodArgList[Index].Name, 
                                   Node->MethodArgList[Index].Type);
                        }
#endif
                        if(Node->MethodReturn)
                        {
                            printf("METHOD RETURNS %s\n", Node->MethodReturn->ClassName);
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        CheckWalk(Node->NonTerminal.Children[Index]);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        CheckWalk(*Iterator);
                    }
                }
            } break;
        }
    }
}

void
PasteInVars(node_ast *Node, FILE *CodeFile)
{
    for(s32 Index = 0;
        Index < Node->VarCount;
        ++Index)
    {
        quack_var QuackArg = Node->VarList[Index];
        if(QuackArg.Type)
        {
            fprintf(CodeFile, "  ");
            fprintf(CodeFile, "obj_%s ", QuackArg.Type->ClassName);
            fprintf(CodeFile, QuackArg.Name);
            fprintf(CodeFile, ";\n");
        }
    }
    
    if(Node->ClassExtend)
    {
        PasteInVars(Node->ClassExtend, CodeFile);
    }
}

void
PasteInMethodArgList(node_ast *MethodNode, FILE *CodeFile)
{
    fprintf(CodeFile, "(");
    
    if(MethodNode->ClassContext)
    {
        if(MethodNode->MethodArgCount == 0)
        {
            fprintf(CodeFile, "obj_%s", MethodNode->ClassContext->ClassName);
        }
        else
        {
            fprintf(CodeFile, "obj_%s, ", MethodNode->ClassContext->ClassName);
        }
    }
    
    for(s32 Index = 0;
        Index < MethodNode->MethodArgCount;
        ++Index)
    {
        quack_arg QuackArg = MethodNode->MethodArgList[Index];
        
        if(QuackArg.Type)
        {
            if(Index == (MethodNode->MethodArgCount - 1))
            {
                fprintf(CodeFile, "obj_%s", QuackArg.Type);
            }
            else
            {
                fprintf(CodeFile, "obj_%s, ", QuackArg.Type);
            }
        }
    }
    fprintf(CodeFile,")");
}

void
PasteInMethodArgListWithName(node_ast *MethodNode, FILE *CodeFile)
{
    fprintf(CodeFile, "(");
    if(MethodNode->MethodArgCount == 0)
    {
        fprintf(CodeFile, "obj_%s this", MethodNode->ClassContext->ClassName);
    }
    else
    {
        fprintf(CodeFile, "obj_%s this, ", MethodNode->ClassContext->ClassName);
    }
    for(s32 Index = 0;
        Index < MethodNode->MethodArgCount;
        ++Index)
    {
        quack_arg QuackArg = MethodNode->MethodArgList[Index];
        
        if(QuackArg.Type)
        {
            if(Index == (MethodNode->MethodArgCount - 1))
            {
                fprintf(CodeFile, "obj_%s %s", QuackArg.Type, QuackArg.Name);
            }
            else
            {
                fprintf(CodeFile, "obj_%s %s, ", QuackArg.Type, QuackArg.Name);
            }
        }
    }
    fprintf(CodeFile,")");
}

void
PasteInClassArgList(node_ast *ClassNode, FILE *CodeFile)
{
    fprintf(CodeFile, "(");
    for(s32 Index = 0;
        Index < ClassNode->ClassArgCount;
        ++Index)
    {
        quack_arg QuackArg = ClassNode->ClassArgList[Index];
        
        if(QuackArg.Type)
        {
            if(Index == (ClassNode->ClassArgCount - 1))
            {
                fprintf(CodeFile, "obj_%s", QuackArg.Type);
            }
            else
            {
                fprintf(CodeFile, "obj_%s, ", QuackArg.Type);
            }
        }
    }
    fprintf(CodeFile,")");
}

void
PasteInClassArgListWithName(node_ast *ClassNode, FILE *CodeFile)
{
    fprintf(CodeFile, "(");
    for(s32 Index = 0;
        Index < ClassNode->ClassArgCount;
        ++Index)
    {
        quack_arg QuackArg = ClassNode->ClassArgList[Index];
        
        if(QuackArg.Type)
        {
            if(Index == (ClassNode->ClassArgCount - 1))
            {
                fprintf(CodeFile, "obj_%s %s", QuackArg.Type, QuackArg.Name);
            }
            else
            {
                fprintf(CodeFile, "obj_%s %s, ", QuackArg.Type, QuackArg.Name);
            }
        }
    }
    fprintf(CodeFile,")");
}

void
PasteInClassConstructorAssignments(node_ast *ClassNode, FILE *CodeFile)
{
    for(s32 Index = 0;
        Index < ClassNode->ClassArgCount;
        ++Index)
    {
        quack_arg QuackArg = ClassNode->ClassArgList[Index];
        
        if(QuackArg.Type)
        {
            fprintf(CodeFile, "  new_thing->%s  = %s;\n", QuackArg.Name, QuackArg.Name);
        }
    }
}

void
PasteInMethodsOfClass(node_ast *Node, FILE *CodeFile, node_ast *BaseNode)
{
    if(Node)
    {
        quack_full_method_list FullList = {};
        GetFullMethodList(Node, &FullList);
        
        for(s32 Index = 0;
            Index < FullList.Count;
            ++Index)
        {
            
            node_ast *MethodNode = 
                FindMethodInClass(FullList.List[Index].Type, FullList.List[Index].Name);
            
            node_ast *MethodIdentNode = MethodNode->NonTerminal.Children[0];
            char *MethodName = MethodIdentNode->Ident.Text;
            
            char *ReturnName = (char *)"Nothing(is this correct?)";
            if(MethodNode->MethodReturn)
            {
                ReturnName = MethodNode->MethodReturn->ClassName;
            }
            
            fprintf(CodeFile, "  ");
            fprintf(CodeFile, "obj_%s (*", ReturnName);
            fprintf(CodeFile, MethodName);
            fprintf(CodeFile, ") ");
            PasteInMethodArgList(MethodNode, CodeFile);
            fprintf(CodeFile, ";\n");
        }
    }
}

void
GenerateRExpression(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_plus:
                    case NonTerminalType_sub:
                    case NonTerminalType_mult:
                    case NonTerminalType_div:
                    {
                        char *OperatorType = (char *)"";
                        switch(Node->NonTerminal.Type)
                        {
                            case NonTerminalType_plus: {OperatorType = (char *)"PLUS";} break;
                            case NonTerminalType_sub: {OperatorType = (char *)"MINUS";} break;
                            case NonTerminalType_mult: {OperatorType = (char *)"TIMES";} break;
                            case NonTerminalType_div: {OperatorType = (char *)"DIVIDE";} break;
                            
                        }
                        
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        GenerateRExpression(Left, CodeFile);
                        fprintf(CodeFile, "->clazz->%s(", OperatorType);
                        GenerateRExpression(Left, CodeFile);
                        fprintf(CodeFile, ", ");
                        GenerateRExpression(Right, CodeFile);
                        fprintf(CodeFile, ")");
                    } break;
                    
                    case NonTerminalType_unary:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        GenerateRExpression(Left, CodeFile);
                        fprintf(CodeFile, "->clazz->UNARY(");
                        GenerateRExpression(Left, CodeFile);
                        fprintf(CodeFile, ")");
                    } break;
                    
                    case NonTerminalType_constructor:
                    {
                        fprintf(CodeFile, "the_class_%s->constructor(", Node->ExpressionType->ClassName);
                        node_ast *ArgListNode = Node->NonTerminal.Children[1];
                        s32 ListLength = ArgListNode->List.List->size();
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ArgListNode->List.List->begin();
                            Iterator != ArgListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ActualArg = (*Iterator);
                                GenerateRExpression(ActualArg, CodeFile);
                                
                                ++Index;
                                if(Index < ListLength)
                                {
                                    fprintf(CodeFile, ", ");
                                }
                            }
                        }
                        fprintf(CodeFile, ")");
                    } break;
                    
                    case NonTerminalType_accessor:
                    {
                        // ENDED HERE TRYING TO FIND OUT WHY ACCESSOR DONES"T SHOW UP
                        fprintf(CodeFile, "ACCESSOR HERE");
                    } break;
                    
                    case NonTerminalType_dot:
                    {
                        if(Node->ExpressionType)
                        {
                            fprintf(CodeFile, "tmp_%d", Node->Tmp);
                        }
                    } break;
                }
            } break;
            
            case AstType_ident:
            {
                fprintf(CodeFile, "tmp_%d", Node->Tmp);
            } break;
            
            case AstType_int_lit:
            {
                fprintf(CodeFile, "tmp_%d", Node->Tmp);
            } break;
        }
    }
}

void
GenerateMethodImplementation(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_if:
                    {
                        
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        
                    } break;
                    
                    case NonTerminalType_return:
                    {
                        
                    } break;
                    
                    case NonTerminalType_gets:
                    {
                        
                    } break;
                    
                    case NonTerminalType_rexpression:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        if(NextNode->ExpressionType)
                        {
                            fprintf(CodeFile, "obj_%s tmp_%d = ", NextNode->ExpressionType->ClassName, 
                                    TmpCounter);
                            Node->Tmp = TmpCounter++;
                            GenerateRExpression(NextNode, CodeFile);
                            fprintf(CodeFile, ";\n");
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        GenerateMethodImplementation(Node->NonTerminal.Children[Index], CodeFile);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateMethodImplementation(*Iterator, CodeFile);
                    }
                }
            } break;
        }
    }
}

void
GenerateRigtSideTempAssignment(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_dot:
                    {
                        if(Node->ExpressionType)
                        {
                            node_ast *Left = Node->NonTerminal.Children[0];
                            node_ast *Right = Node->NonTerminal.Children[1];
                            char *AccessName = Right->Ident.Text;
                            GenerateRigtSideTempAssignment(Left, CodeFile);
                            fprintf(CodeFile, "->%s", AccessName);
                        }
                    } break;
                }
            } break;
            
            case AstType_ident:
            {
                fprintf(CodeFile, "%s", Node->Ident.Text);
            } break;
            
            case AstType_int_lit:
            {
                fprintf(CodeFile, "%s", Node->IntLit.Text);
            } break;
        }
    }
}

void
GenerateTempVars(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_dot:
                    {
                        if(Node->ExpressionType)
                        {
                            fprintf(CodeFile, "obj_%s tmp_%d = ", Node->ExpressionType->ClassName, 
                                    TmpCounter);
                            Node->Tmp = TmpCounter++;
                            GenerateRigtSideTempAssignment(Node, CodeFile);
                            fprintf(CodeFile, ";\n");
                        }
                    } break;
                    
                    case NonTerminalType_accessor:
                    {
                        node_ast *AccessArgumentListNode = Node->NonTerminal.Children[2];
                        
                    } break;
                    
                    case NonTerminalType_constructor:
                    {
                        if(Node->ExpressionType)
                        {
                            node_ast *ConstructorArgumentListNode = Node->NonTerminal.Children[1];
                            if(ConstructorArgumentListNode)
                            {
                                list<node_ast *>::const_iterator Iterator;
                                for(Iterator = ConstructorArgumentListNode->List.List->begin();
                                    Iterator != ConstructorArgumentListNode->List.List->end();
                                    ++Iterator)
                                {
                                    if(*Iterator)
                                    {
                                        node_ast *ArgNode = (*Iterator);
                                        if(ArgNode->ExpressionType)
                                        {
                                            GenerateTempVars(ArgNode, CodeFile);
                                        }
                                    }
                                }
                                
                                fprintf(CodeFile, "obj_%s tmp_%d = ", Node->ExpressionType->ClassName, 
                                        TmpCounter);
                                Node->Tmp = TmpCounter++;
                                fprintf(CodeFile, "the_class_%s->constructor(");
                                
                                s32 Index = 0;
                                s32 ListSize = ConstructorArgumentListNode->List.List->size();
                                for(Iterator = ConstructorArgumentListNode->List.List->begin();
                                    Iterator != ConstructorArgumentListNode->List.List->end();
                                    ++Iterator)
                                {
                                    if(*Iterator)
                                    {
                                        node_ast *ArgNode = (*Iterator);
                                        if(ArgNode->ExpressionType)
                                        {
                                            if(Index < (ListSize - 1))
                                            {
                                                fprintf(CodeFile, "tmp_%d, ", ArgNode->Tmp);
                                            }
                                            else
                                            {
                                                fprintf(CodeFile, "tmp_%d", ArgNode->Tmp);
                                            }
                                        }
                                        
                                        ++Index;
                                    }
                                }
                                
                                fprintf(CodeFile, ");\n");
                            }
                        }
                    } break;
                }
            } break;
            
            case AstType_int_lit:
            {
                fprintf(CodeFile, "obj_Int tmp_%d = new_Int();\n", TmpCounter);
                fprintf(CodeFile, "tmp_%d->value = %s;\n", TmpCounter, Node->IntLit.Text);
                Node->Tmp = TmpCounter++;
            } break;
            
            case AstType_ident:
            {
                // Don't think we need anything here
            } break;
        }
    }
}

void
GenerateAllTempVars(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_if:
                    {
                        
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        
                    } break;
                    
                    case NonTerminalType_return:
                    {
                        
                    } break;
                    
                    case NonTerminalType_gets:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[2];
                        
                        GenerateTempVars(Right, CodeFile);
                        
                    } break;
                    
                    case NonTerminalType_rexpression:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        if(NextNode->ExpressionType)
                        {
                            GenerateTempVars(NextNode, CodeFile);
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        GenerateAllTempVars(Node->NonTerminal.Children[Index], CodeFile);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateAllTempVars(*Iterator, CodeFile);
                    }
                }
            } break;
        }
    }
}

void
GenerateBoolExpression(node_ast *Node, FILE *CodeFile, s32 Temp, s32 TempEnd)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_and:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        GenerateBoolExpression(Left, CodeFile, Temp, TempEnd);
                        GenerateBoolExpression(Right, CodeFile, Temp, TempEnd);
                        
                        fprintf(CodeFile, "if(!(");
                        fprintf(CodeFile, ")) { goto tmp_%d_label_end; }\n", TempEnd);
                        fprintf(CodeFile, "if(");
                        fprintf(CodeFile, ") { goto Bla; }\n");
                    }
                }
            } break;
            
            case AstType_int_lit:
            {
                
            } break;
            
            case AstType_ident:
            {
                
            } break;
        }
    }
}

node_ast *
GetMethodForCast(node_ast *RootNode, node_ast *Rexpression, char *MethodName)
{
    node_ast *MethodNode = 0;
    
    node_ast *FoundNode = FindAstByClassName(RootNode, 
                                             Rexpression->ExpressionType->ClassName);
    b32 FoundMethod = false;
    if(FoundNode)
    {
        quack_full_method_list FullList = {};
        GetFullMethodList(FoundNode, &FullList);
        
        for(s32 Index = 0;
            Index < FullList.Count;
            ++Index)
        {
            MethodNode = FindMethodInClass(FullList.List[Index].Type, 
                                           FullList.List[Index].Name);
            
            if(CompareStrings(MethodNode->MethodName, MethodName))
            {
                FoundMethod = true;
                
                break;
            }
        }
    }
    
    return(MethodNode);
}

void
GenerateExpression(node_ast *Node, FILE *CodeFile, char *CastType)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_plus:
                    case NonTerminalType_sub:
                    case NonTerminalType_mult:
                    case NonTerminalType_div:
                    {
                        char *OperatorForType = (char *)"";
                        switch(Node->NonTerminal.Type)
                        {
                            case NonTerminalType_plus: {OperatorForType = (char *)"PLUS";} break;
                            case NonTerminalType_sub: {OperatorForType = (char *)"MINUS";} break;
                            case NonTerminalType_mult: {OperatorForType = (char *)"TIMES";} break;
                            case NonTerminalType_div: {OperatorForType = (char *)"DIVIDE";} break;
                        }
                        
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        node_ast *MethodNode = GetMethodForCast(RootNode, Left, 
                                                                OperatorForType);
                        if(MethodNode && MethodNode->MethodReturn)
                        {
                            Node->Tmp = TmpCounter++;
                            
                            GenerateExpression(Left, CodeFile, MethodNode->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            GenerateExpression(Right, CodeFile, MethodNode->MethodArgList[1].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "obj_%s tmp_%d = ", 
                                    MethodNode->MethodReturn->ClassName, 
                                    Node->Tmp);
                            fprintf(CodeFile, "tmp_%d->clazz->%s(", Left->Tmp, OperatorForType);
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->ClassContext->ClassName,
                                    Left->Tmp);
                            fprintf(CodeFile, ", ");
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->MethodArgList[0].Type,
                                    Right->Tmp);
                            fprintf(CodeFile, ");\n");
                        }
                    } break;
                    
                    case NonTerminalType_unary:
                    {
                        char *OperatorForType = (char *)"UNARY";
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *MethodNode = GetMethodForCast(RootNode, Left, 
                                                                OperatorForType);
                        if(MethodNode && MethodNode->MethodReturn)
                        {
                            Node->Tmp = TmpCounter++;
                            
                            GenerateExpression(Left, CodeFile, MethodNode->MethodArgList[0].Type);
                            fprintf(CodeFile, "obj_%s tmp_%d = ", 
                                    MethodNode->MethodReturn->ClassName, 
                                    Node->Tmp);
                            fprintf(CodeFile, "tmp_%d->clazz->%s(", Left->Tmp, OperatorForType);
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->MethodArgList[0].Type,
                                    Left->Tmp);
                            fprintf(CodeFile, ");\n");
                        }
                    } break;
                    
                    case NonTerminalType_equals:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        node_ast *MethodNode = GetMethodForCast(RootNode, Left, 
                                                                (char *)"EQUALS");
                        if(MethodNode && MethodNode->MethodReturn)
                        {
                            Node->Tmp = TmpCounter++;
                            
                            GenerateExpression(Left, CodeFile, MethodNode->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            GenerateExpression(Right, CodeFile, MethodNode->MethodArgList[1].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "obj_%s tmp_%d = ", 
                                    MethodNode->MethodReturn->ClassName, 
                                    Node->Tmp);
                            fprintf(CodeFile, "tmp_%d->clazz->EQUALS(", Left->Tmp);
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->ClassContext->ClassName,
                                    Left->Tmp);
                            fprintf(CodeFile, ", ");
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->MethodArgList[0].Type,
                                    Right->Tmp);
                            fprintf(CodeFile, ");\n");
                        }
                    } break;
                    
                    case NonTerminalType_more:
                    case NonTerminalType_less:
                    case NonTerminalType_atmost:
                    case NonTerminalType_atleast:
                    {
                        char *OperatorForType = (char *)"";
                        switch(Node->NonTerminal.Type)
                        {
                            case NonTerminalType_more: {OperatorForType = (char *)"MORE";} break;
                            case NonTerminalType_less: {OperatorForType = (char *)"LESS";} break;
                            case NonTerminalType_atmost: {OperatorForType = (char *)"ATMOST";} break;
                            case NonTerminalType_atleast: {OperatorForType = (char *)"ATLEAST";} break;
                        }
                        
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        node_ast *MethodNode = GetMethodForCast(RootNode, Left, 
                                                                OperatorForType);
                        if(MethodNode && MethodNode->MethodReturn)
                        {
                            Node->Tmp = TmpCounter++;
                            
                            GenerateExpression(Left, CodeFile, MethodNode->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            GenerateExpression(Right, CodeFile, MethodNode->MethodArgList[1].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "obj_%s tmp_%d = ", 
                                    MethodNode->MethodReturn->ClassName, 
                                    Node->Tmp);
                            fprintf(CodeFile, "tmp_%d->clazz->%s(", Left->Tmp, OperatorForType);
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->ClassContext->ClassName,
                                    Left->Tmp);
                            fprintf(CodeFile, ", ");
                            fprintf(CodeFile, "(obj_%s)tmp_%d", 
                                    MethodNode->MethodArgList[0].Type,
                                    Right->Tmp);
                            fprintf(CodeFile, ");\n");
                        }
                    } break;
                    
                    case NonTerminalType_and:
                    {
                        Node->Tmp = TmpCounter++;
                        Node->TmpEnd = TmpCounter++;
                        fprintf(CodeFile, "obj_Boolean tmp_%d = lit_false; // Result for AND\n", Node->Tmp);
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        node_ast *MethodNodeLeft = GetMethodForCast(RootNode, Left, 
                                                                    (char *)"EQUALS");
                        node_ast *MethodNodeRight = GetMethodForCast(RootNode, Right, 
                                                                     (char *)"EQUALS");
                        if(MethodNodeLeft && MethodNodeRight)
                        {
                            GenerateExpression(Left, CodeFile, MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d, (obj_%s)lit_false)->value)",
                                    Left->Tmp, 
                                    MethodNodeLeft->ClassContext->ClassName,
                                    Left->Tmp,
                                    MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, " { goto label_%d; }\n", Node->TmpEnd);
                            
                            GenerateExpression(Right, CodeFile, MethodNodeRight->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d, (obj_%s)lit_false)->value)",
                                    Right->Tmp, 
                                    MethodNodeLeft->ClassContext->ClassName,
                                    Right->Tmp,
                                    MethodNodeRight->MethodArgList[0].Type);
                            fprintf(CodeFile, " { goto label_%d; }\n", Node->TmpEnd);
                            
                            fprintf(CodeFile, "tmp_%d = lit_true;\n", Node->Tmp);
                            
                            fprintf(CodeFile, "label_%d:\n", Node->TmpEnd);
                        }
                    } break;
                    
                    case NonTerminalType_or:
                    {
                        Node->Tmp = TmpCounter++;
                        Node->TmpEnd = TmpCounter++;
                        fprintf(CodeFile, "obj_Boolean tmp_%d = lit_true; // Result for OR\n", Node->Tmp);
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        node_ast *MethodNodeLeft = GetMethodForCast(RootNode, Left, 
                                                                    (char *)"EQUALS");
                        node_ast *MethodNodeRight = GetMethodForCast(RootNode, Right, 
                                                                     (char *)"EQUALS");
                        if(MethodNodeLeft && MethodNodeRight)
                        {
                            GenerateExpression(Left, CodeFile, MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d, (obj_%s)lit_true)->value)",
                                    Left->Tmp, 
                                    MethodNodeLeft->ClassContext->ClassName,
                                    Left->Tmp,
                                    MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, " { goto label_%d; }\n", Node->TmpEnd);
                            
                            GenerateExpression(Right, CodeFile, MethodNodeRight->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d, (obj_%s)lit_true)->value)",
                                    Right->Tmp, 
                                    MethodNodeLeft->ClassContext->ClassName,
                                    Right->Tmp,
                                    MethodNodeRight->MethodArgList[0].Type);
                            fprintf(CodeFile, " { goto label_%d; }\n", Node->TmpEnd);
                            
                            fprintf(CodeFile, "tmp_%d = lit_false;\n", Node->Tmp);
                            
                            fprintf(CodeFile, "label_%d:\n", Node->TmpEnd);
                        }
                    } break;
                    
                    case NonTerminalType_not:
                    {
                        Node->Tmp = TmpCounter++;
                        Node->TmpEnd = TmpCounter++;
                        fprintf(CodeFile, "obj_Boolean tmp_%d = lit_false; // Result for NOT\n", Node->Tmp);
                        node_ast *Left = Node->NonTerminal.Children[0];
                        
                        node_ast *MethodNodeLeft = GetMethodForCast(RootNode, Left, 
                                                                    (char *)"EQUALS");
                        
                        if(MethodNodeLeft)
                        {
                            GenerateExpression(Left, CodeFile, MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, ";\n");
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d, (obj_%s)lit_true)->value)",
                                    Left->Tmp, 
                                    MethodNodeLeft->ClassContext->ClassName,
                                    Left->Tmp,
                                    MethodNodeLeft->MethodArgList[0].Type);
                            fprintf(CodeFile, " { goto label_%d; }\n", Node->TmpEnd);
                            
                            fprintf(CodeFile, "tmp_%d = lit_true;\n", Node->Tmp);
                            
                            fprintf(CodeFile, "label_%d:\n", Node->TmpEnd);
                        }
                    } break;
                    
                    case NonTerminalType_constructor:
                    {
                        Node->Tmp = TmpCounter++;
                        
                        node_ast *ArgListNode = Node->NonTerminal.Children[1];
                        s32 ListLength = ArgListNode->List.List->size();
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ArgListNode->List.List->begin();
                            Iterator != ArgListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ActualArg = (*Iterator);
                                if(ActualArg->ExpressionType)
                                {
                                    GenerateExpression(ActualArg, CodeFile, 
                                                       ActualArg->ExpressionType->ClassName);
                                    fprintf(CodeFile, ";\n");
                                }
                            }
                        }
                        fprintf(CodeFile, "obj_%s tmp_%d = ", Node->ExpressionType->ClassName, Node->Tmp);
                        fprintf(CodeFile, "the_class_%s->constructor(", Node->ExpressionType->ClassName);
                        
                        Index = 0;
                        for(Iterator = ArgListNode->List.List->begin();
                            Iterator != ArgListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ActualArg = (*Iterator);
                                fprintf(CodeFile, "tmp_%d", ActualArg->Tmp);
                                
                                ++Index;
                                if(Index < ListLength)
                                {
                                    fprintf(CodeFile, ", ");
                                }
                            }
                        }
                        fprintf(CodeFile, ")");
                    } break;
                    
                    case NonTerminalType_accessor:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        node_ast *ArgListNode = Node->NonTerminal.Children[2];
                        
                        s32 ListLength = ArgListNode->List.List->size();
                        s32 Index = 0;
                        list<node_ast *>::const_iterator Iterator;
                        for(Iterator = ArgListNode->List.List->begin();
                            Iterator != ArgListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ActualArg = (*Iterator);
                                if(ActualArg->ExpressionType)
                                {
                                    GenerateExpression(ActualArg, CodeFile, 
                                                       ActualArg->ExpressionType->ClassName);
                                    fprintf(CodeFile, ";\n");
                                }
                            }
                        }
                        
                        GenerateExpression(Left, CodeFile, 0);
                        fprintf(CodeFile, "->clazz->%s(", Right->Ident.Text);
                        
                        if(Left->ExpressionType)
                        {
                            node_ast *FoundMethod = 
                                FindMethodInClassAndFollowParent(Left->ExpressionType, Right->Ident.Text);
                            
                            if(FoundMethod)
                            {
                                fprintf(CodeFile, "(obj_%s)", FoundMethod->ClassContext->ClassName);
                            }
                        }
                        
                        GenerateExpression(Left, CodeFile, 0);
                        if(ListLength > 0)
                        {
                            fprintf(CodeFile, ", ");
                        }
                        
                        Index = 0;
                        for(Iterator = ArgListNode->List.List->begin();
                            Iterator != ArgListNode->List.List->end();
                            ++Iterator)
                        {
                            if(*Iterator)
                            {
                                node_ast *ActualArg = (*Iterator);
                                fprintf(CodeFile, "tmp_%d", ActualArg->Tmp);
                                
                                ++Index;
                                if(Index < ListLength)
                                {
                                    fprintf(CodeFile, ", ");
                                }
                            }
                        }
                        
                        fprintf(CodeFile, ")");
                    } break;
                    
                    case NonTerminalType_dot:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *Right = Node->NonTerminal.Children[1];
                        
                        GenerateExpression(Left, CodeFile, 0);
                        fprintf(CodeFile, "->%s", Right->Ident.Text);
                    } break;
                }
            } break;
            
            case AstType_int_lit:
            {
                char *CT = 0;
                if(!CastType)
                {
                    CT = (char *)"Int";
                }
                else
                {
                    CT = CastType;
                }
                Node->Tmp = TmpCounter++;
                fprintf(CodeFile, "obj_%s tmp_%d = (obj_%s)int_literal(%s)", 
                        CT, Node->Tmp, CT, Node->Ident.Text);
            } break;
            
            case AstType_ident:
            {
                Node->Tmp = TmpCounter++;
                Node->BlankIdent = false;
                
                if(CompareStrings(Node->Ident.Text, (char *)"true"))
                {
                    char *CT = 0;
                    if(!CastType)
                    {
                        CT = (char *)"Boolean";
                    }
                    else
                    {
                        CT = CastType;
                    }
                    fprintf(CodeFile, "obj_%s tmp_%d = (obj_%s)lit_true", CT, Node->Tmp,
                            CastType);
                }
                else if(CompareStrings(Node->Ident.Text, (char *)"false"))
                {
                    char *CT = 0;
                    if(!CastType)
                    {
                        CT = (char *)"Boolean";
                    }
                    else
                    {
                        CT = CastType;
                    }
                    fprintf(CodeFile, "obj_%s tmp_%d = (obj_%s)lit_false", CT, Node->Tmp,
                            CastType);
                }
                else
                {
                    fprintf(CodeFile, "%s", 
                            Node->Ident.Text);
                    Node->BlankIdent = true;
                    
#if 0
                    node_ast *CurrentContext = 0;
                    if(Node->MethodContext)
                    {
                        CurrentContext = Node->MethodContext;
                    }
                    else if(Node->ClassContext)
                    {
                        CurrentContext = Node->ClassContext;
                    }
                    else
                    {
                        CurrentContext = GlobalContext;
                    }
                    
                    FindVarInContextAndParentContexts();
#endif
                    
                }
                
            } break;
        }
    }
}

void GenerateGlobalStatments(node_ast *StatementListNode, FILE *CodeFile, node_ast *RootNode);

void
GenerateStatements(node_ast *Node, FILE *CodeFile, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_gets:
                    {
                        node_ast *Left = Node->NonTerminal.Children[0];
                        node_ast *OptionalType = Node->NonTerminal.Children[1];
                        node_ast *Right = Node->NonTerminal.Children[2];
                        
                        GenerateExpression(Right, CodeFile, Right->ExpressionType->ClassName);
                        fprintf(CodeFile, ";\n");
                        
                        if(CompareStrings(OptionalType->Ident.Text, (char *)"Nothing"))
                        {
                            fprintf(CodeFile, "obj_%s ", Right->ExpressionType->ClassName);
                            Left->ExpressionType = Right->ExpressionType;
                        }
                        else
                        {
                            fprintf(CodeFile, "obj_%s ", OptionalType->Ident.Text);
                            node_ast *OptionalNode = FindAstByClassName(RootNode, 
                                                                        OptionalType->Ident.Text);
                            if(OptionalNode)
                            {
                                Left->ExpressionType = OptionalNode;
                            }
                        }
                        GenerateExpression(Left, CodeFile, 0);
                        
                        if(CompareStrings(OptionalType->Ident.Text, (char *)"Nothing"))
                        {
                            fprintf(CodeFile, " = tmp_%d;\n", Right->Tmp);
                        }
                        else
                        {
                            fprintf(CodeFile, " = (obj_%s)tmp_%d;\n", OptionalType->Ident.Text,
                                    Right->Tmp);
                        }
                    } break;
                    
                    case NonTerminalType_if:
                    {
                        node_ast *Rexpression = Node->NonTerminal.Children[0];
                        node_ast *StatementListNode = Node->NonTerminal.Children[1];
                        node_ast *ElifListNode = Node->NonTerminal.Children[2];
                        
                        node_ast *MethodNode = GetMethodForCast(RootNode, Rexpression, 
                                                                (char *)"EQUALS");
                        
                        
                        if(MethodNode)
                        {
                            GenerateExpression(Rexpression, CodeFile, 0);
                            fprintf(CodeFile, ";\n");
                            
                            list<node_ast *>::const_iterator Iterator;
                            for(Iterator = ElifListNode->List.List->begin();
                                Iterator != ElifListNode->List.List->end();
                                ++Iterator)
                            {
                                if(*Iterator)
                                {
                                    node_ast *ElifNode = (*Iterator);
                                    GenerateExpression(ElifNode, CodeFile, 0);
                                    ++Iterator;
                                }
                            }
                            
                            fprintf(CodeFile, "if(tmp_%d->value)\n{ \n",
                                    Rexpression->Tmp, 
                                    MethodNode->ClassContext->ClassName,
                                    Rexpression->Tmp,
                                    MethodNode->MethodArgList[0].Type);
                            GenerateGlobalStatments(StatementListNode, CodeFile, RootNode);
                            fprintf(CodeFile, "}\n");
                            
                            
                            list<node_ast *>::const_iterator IteratorB;
                            for(IteratorB = ElifListNode->List.List->begin();
                                IteratorB != ElifListNode->List.List->end();
                                ++IteratorB)
                            {
                                if(*IteratorB)
                                {
                                    node_ast *ElifNode = (*IteratorB);
                                    fprintf(CodeFile, "else if(tmp_%d->value)\n{ \n",
                                            ElifNode->Tmp, 
                                            MethodNode->ClassContext->ClassName,
                                            ElifNode->Tmp,
                                            MethodNode->MethodArgList[0].Type);
                                    
                                    ++IteratorB;
                                    if(*IteratorB)
                                    {
                                        node_ast *ElifStatementNode = (*IteratorB);
                                        GenerateGlobalStatments(ElifStatementNode, CodeFile, RootNode);
                                    }
                                    
                                    fprintf(CodeFile, "}\n");
                                }
                            }
                            
                            node_ast *ElseNode = Node->NonTerminal.Children[3];
                            fprintf(CodeFile, "else\n{\n");
                            GenerateGlobalStatments(ElseNode, CodeFile, RootNode);
                            fprintf(CodeFile, "}\n");
                            
                            fprintf(CodeFile, "\n");
                        }
                    } break;
                    
                    case NonTerminalType_while:
                    {
                        node_ast *Rexpression = Node->NonTerminal.Children[0];
                        node_ast *StatementListNode = Node->NonTerminal.Children[1];
                        
                        node_ast *MethodNode = GetMethodForCast(RootNode, Rexpression, 
                                                                (char *)"EQUALS");
                        if(MethodNode)
                        {
                            fprintf(CodeFile, "while(1){\n");
                            
                            GenerateExpression(Rexpression, CodeFile, 0);
                            fprintf(CodeFile, "if(tmp_%d->clazz->EQUALS((obj_%s)tmp_%d ,(obj_%s)lit_false)->value){ break; }\n", Rexpression->Tmp, 
                                    MethodNode->ClassContext->ClassName,
                                    Rexpression->Tmp,
                                    MethodNode->MethodArgList[0].Type);
                            
                            GenerateGlobalStatments(StatementListNode, CodeFile, RootNode);
                            
                            fprintf(CodeFile, "}\n");
                        }
                    } break;
                    
                    case NonTerminalType_rexpression:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        
                        GenerateExpression(NextNode, CodeFile, 0);
                        fprintf(CodeFile, ";\n");
                    } break;
                    
                    case NonTerminalType_return:
                    {
                        node_ast *NextNode = Node->NonTerminal.Children[0];
                        
                        node_ast *MethodNode = Node->MethodContext;
                        if(MethodNode)
                        {
                            if(MethodNode->MethodReturn)
                            {
                                GenerateExpression(NextNode, CodeFile, MethodNode->MethodReturn->ClassName);
                                fprintf(CodeFile, ";\n");
                                fprintf(CodeFile, "return tmp_%d;\n", NextNode->Tmp);
                            }
                        }
                    }
                }
            } break;
        }
    }
}

void
GenerateMethodStatements(node_ast *StatementListNode, FILE *CodeFile, node_ast *RootNode)
{
    if(StatementListNode)
    {
        switch(StatementListNode->AstType)
        {
            case AstType_non_terminal:
            {
                
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = StatementListNode->List.List->rbegin();
                    Iterator != StatementListNode->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateStatements(*Iterator, CodeFile, RootNode);
                        fprintf(CodeFile, "\n");
                    }
                }
            } break;
        }
    }
}

void
GenerateGlobalStatments(node_ast *StatementListNode, FILE *CodeFile, node_ast *RootNode)
{
    if(StatementListNode)
    {
        switch(StatementListNode->AstType)
        {
            case AstType_non_terminal:
            {
                
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = StatementListNode->List.List->rbegin();
                    Iterator != StatementListNode->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateStatements(*Iterator, CodeFile, RootNode);
                        fprintf(CodeFile, "\n");
                    }
                }
            } break;
        }
    }
}

void
GenerateDefCode(node_ast *Node, FILE *CodeFile, node_ast *RootNode)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        if(!(CompareStrings(Node->ClassContext->ClassName, (char *)"Obj") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"String") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Boolean") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Int") || 
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Nothing")))
                        {
                            fprintf(CodeFile, "typedef struct obj_%s_struct {\n", Node->ClassName);
                            fprintf(CodeFile, "  class_%s clazz;\n", Node->ClassName);
                            PasteInVars(Node, CodeFile);
                            fprintf(CodeFile, "} * obj_%s;\n\n", Node->ClassName);
                            
                            fprintf(CodeFile, "struct class_%s_struct {\n", Node->ClassName);
                            fprintf(CodeFile, "  obj_%s (*constructor) ", Node->ClassName);
                            PasteInClassArgList(Node, CodeFile);
                            fprintf(CodeFile, ";\n");
                            PasteInMethodsOfClass(Node, CodeFile, Node);
                            fprintf(CodeFile, "};\n\n");
                            
                            fprintf(CodeFile, "extern class_%s the_class_%s;\n\n", 
                                    Node->ClassName, Node->ClassName);
                            
                            fprintf(CodeFile, "obj_%s new_%s", 
                                    Node->ClassName, Node->ClassName);
                            PasteInClassArgListWithName(Node, CodeFile);
                            fprintf(CodeFile, " {\n");
                            fprintf(CodeFile, 
                                    "  obj_%s new_thing = (obj_%s)malloc(sizeof(struct obj_%s_struct));\n", 
                                    Node->ClassName, 
                                    Node->ClassName, 
                                    Node->ClassName);
                            fprintf(CodeFile, "  new_thing->clazz = the_class_%s;\n", Node->ClassName);
                            PasteInClassConstructorAssignments(Node, CodeFile);
                            fprintf(CodeFile, "  return new_thing;\n");
                            
                            fprintf(CodeFile, "}\n\n");
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        if(!(CompareStrings(Node->ClassContext->ClassName, (char *)"Obj") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"String") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Boolean") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Int") || 
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Nothing")))
                        {
                            // TODO(tucker): allow for things to have no returns!
                            if(Node->MethodReturn)
                            {
                                fprintf(CodeFile, "obj_%s %s_method_%s", 
                                        Node->MethodReturn->ClassName, 
                                        Node->ClassContext->ClassName, 
                                        Node->MethodName);
                                PasteInMethodArgListWithName(Node, CodeFile);
                                fprintf(CodeFile, " {\n");
                                
                                //fprintf(CodeFile, "METHOD IMPLEMENATION\n");
                                
                                //GenerateAllTempVars(Node, CodeFile);
                                fprintf(CodeFile, "\n");
                                //GenerateMethodImplementation(Node, CodeFile);
                                node_ast *StatementBlockNode = Node->NonTerminal.Children[3];
                                if(StatementBlockNode)
                                {
                                    GenerateMethodStatements(StatementBlockNode, CodeFile, RootNode);
                                }
                                
                                fprintf(CodeFile, "}\n\n");
                            }
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        GenerateDefCode(Node->NonTerminal.Children[Index], CodeFile, RootNode);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateDefCode(*Iterator, CodeFile, RootNode);
                    }
                }
            } break;
        }
    }
}

void
PasteMethodsForSingleton(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        quack_full_method_list FullList = {};
                        GetFullMethodList(Node, &FullList);
                        
                        for(s32 Index = 0;
                            Index < FullList.Count;
                            ++Index)
                        {
                            node_ast *MethodNode = 
                                FindMethodInClass(FullList.List[Index].Type, FullList.List[Index].Name);
                            
                            node_ast *MethodIdentNode = MethodNode->NonTerminal.Children[0];
                            char *MethodName = MethodIdentNode->Ident.Text;
                            
                            if(Index == (FullList.Count-1))
                            {
                                fprintf(CodeFile, " %s_method_%s \n", 
                                        MethodNode->ClassContext->ClassName, MethodName);
                            }
                            else
                            {
                                fprintf(CodeFile, " %s_method_%s, \n", 
                                        MethodNode->ClassContext->ClassName, MethodName);
                            }
                        }
                        
                    } break;
                }
            } break;
        }
    }
}

void
GenerateDefCodeBottom(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        if(!(CompareStrings(Node->ClassContext->ClassName, (char *)"Obj") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"String") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Boolean") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Int") || 
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Nothing")))
                        {
                            fprintf(CodeFile, "struct class_%s_struct the_class_%s_struct = {\n",
                                    Node->ClassName, Node->ClassName);
                            fprintf(CodeFile, "  new_%s, \n", Node->ClassName);
                            PasteMethodsForSingleton(Node, CodeFile);
                            fprintf(CodeFile, "};\n\n");
                            
                            fprintf(CodeFile, "class_%s the_class_%s = &the_class_%s_struct;\n\n",
                                    Node->ClassName, Node->ClassName, Node->ClassName);
                        }
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        GenerateDefCodeBottom(Node->NonTerminal.Children[Index], CodeFile);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateDefCodeBottom(*Iterator, CodeFile);
                    }
                }
            } break;
        }
    }
}

void
GenerateHeaderCode(node_ast *Node, FILE *CodeFile)
{
    if(Node)
    {
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                switch(Node->NonTerminal.Type)
                {
                    case NonTerminalType_class:
                    {
                        if(!(CompareStrings(Node->ClassContext->ClassName, (char *)"Obj") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"String") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Boolean") ||
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Int") || 
                             CompareStrings(Node->ClassContext->ClassName, (char *)"Nothing")))
                        {
                            fprintf(CodeFile, "struct class_%s_struct;\n", Node->ClassName);
                            fprintf(CodeFile, "typedef struct class_%s_struct* class_%s;\n\n", 
                                    Node->ClassName, Node->ClassName);
                        }
                    } break;
                    
                    case NonTerminalType_method:
                    {
                        
                    } break;
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        GenerateHeaderCode(Node->NonTerminal.Children[Index], CodeFile);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::reverse_iterator Iterator;
                for(Iterator = Node->List.List->rbegin();
                    Iterator != Node->List.List->rend();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        GenerateHeaderCode(*Iterator, CodeFile);
                    }
                }
            } break;
        }
    }
}

node_ast *
CreateNewClass(char *Name, char *ExtendName)
{
    char *ActualExtendName = ExtendName;
    if(!ExtendName)
    {
        ActualExtendName = (char *)"";
    }
    
    node_ast *ClassSig = NewNonTerminalNode(NonTerminalType_class_signature, 
                                            3, 
                                            NewIdentNode(Name), 
                                            NewListNode(ListType_argument), 
                                            NewIdentNode(ActualExtendName));
    node_ast *ClassBody = NewNonTerminalNode(NonTerminalType_class_body, 2, 
                                             NewListNode(ListType_statement), 
                                             NewListNode(ListType_method));
    node_ast *Class = NewNonTerminalNode(NonTerminalType_class, 2, 
                                         ClassSig, ClassBody);
    
    return(Class);
}

node_ast *
CreateNewMethod(char *Name, char *ReturnName)
{
    node_ast *StatementBlockNode = NewListNode(ListType_statement);
    node_ast *OptionalTypeNode = NewIdentNode(ReturnName);
    node_ast *ArgumentListNode = NewListNode(ListType_argument);
    
    node_ast *MethodNode = NewNonTerminalNode(NonTerminalType_method, 
                                              4, NewIdentNode(Name),
                                              ArgumentListNode, 
                                              OptionalTypeNode, 
                                              StatementBlockNode);
    return(MethodNode);
}

void 
AddArgumentToNewMethod(node_ast *MethodNode, char *ArgName, char *ArgType)
{
    node_ast *ArgumentListNode = MethodNode->NonTerminal.Children[1];
    
    if(ArgumentListNode)
    {
        node_ast *TypeNode = NewNonTerminalNode(NonTerminalType_type, 2, 
                                                NewIdentNode(ArgName), NewIdentNode(ArgType));
        ArgumentListNode->List.List->push_back(TypeNode);
    }
}

void 
AddArgumentToNewClass(node_ast *ClassNode, char *ArgName, char *ArgType)
{
    node_ast *ClassSignatureNode = ClassNode->NonTerminal.Children[0];
    node_ast *ArgumentListNode = ClassSignatureNode->NonTerminal.Children[1];
    
    if(ArgumentListNode)
    {
        node_ast *TypeNode = NewNonTerminalNode(NonTerminalType_type, 2, 
                                                NewIdentNode(ArgName), NewIdentNode(ArgType));
        ArgumentListNode->List.List->push_back(TypeNode);
    }
}

void
AddNewMethodToNewClass(node_ast *NewClass, node_ast *NewMethod)
{
    node_ast *ClassBody = NewClass->NonTerminal.Children[1];
    if(ClassBody)
    {
        node_ast *MethodListNode = ClassBody->NonTerminal.Children[1];
        MethodListNode->List.List->push_back(NewMethod);
    }
}

int
main(int argc, char **argv)
{
#if 0
#ifdef YYDEBUG
    extern int yydebug;
    yydebug = 1;
#endif
#endif
    
    if(argc > 1)
    {
        FILE *File = fopen(argv[1], "r");
        
        if(File)
        {
            //printf("Beginning parse of %s\n", argv[1]);
            yyin = File;
            int Result = yyparse();
            
            if(Result ==  0)
            {
                //printf("Finished parse with no errors\n");
            }
            else
            {
                printf("Finished parse with some errors\n");
            }
            
            list<node_ast *> *ClassList = RootNode->NonTerminal.Children[0]->List.List;
            node_ast *ClassListNode = RootNode->NonTerminal.Children[0];
            
            //Obj
            node_ast *ObjClass = CreateNewClass((char *)"Obj", 0);
            ClassListNode->List.List->push_back(ObjClass);
            
            node_ast *ObjEqual = CreateNewMethod((char *)"EQUALS", (char *)"Boolean");
            //AddArgumentToNewMethod(ObjEqual, (char *)"this", (char *)"Obj");
            AddArgumentToNewMethod(ObjEqual, (char *)"other", (char *)"Obj");
            AddNewMethodToNewClass(ObjClass, ObjEqual);
            
            node_ast *ObjPrint = CreateNewMethod((char *)"PRINT", (char *)"Obj");
            //AddArgumentToNewMethod(ObjPrint, (char *)"this", (char *)"Obj");
            AddNewMethodToNewClass(ObjClass, ObjPrint);
            
            node_ast *ObjSTR = CreateNewMethod((char *)"STRING", (char *)"String");
            //AddArgumentToNewMethod(ObjSTR, (char *)"this", (char *)"Obj");
            AddNewMethodToNewClass(ObjClass, ObjSTR);
            
            //Int
            node_ast *IntClass = CreateNewClass((char *)"Int", (char *)"Obj");
            ClassListNode->List.List->push_back(IntClass);
            
            node_ast *IntUnary = CreateNewMethod((char *)"UNARY", (char *)"Int");
            //AddArgumentToNewMethod(IntUnary, (char *)"this", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntUnary);
            
            node_ast *IntDivide = CreateNewMethod((char *)"DIVIDE", (char *)"Int");
            //AddArgumentToNewMethod(IntDivide, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntDivide, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntDivide);
            
            node_ast *IntTimes = CreateNewMethod((char *)"TIMES", (char *)"Int");
            //AddArgumentToNewMethod(IntTimes, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntTimes, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntTimes);
            
            node_ast *IntMinus = CreateNewMethod((char *)"MINUS", (char *)"Int");
            //AddArgumentToNewMethod(IntMinus, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntMinus, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntMinus);
            
            node_ast *IntPlus = CreateNewMethod((char *)"PLUS", (char *)"Int");
            //AddArgumentToNewMethod(IntPlus, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntPlus, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntPlus);
            
            node_ast *IntAtleast = CreateNewMethod((char *)"ATLEAST", (char *)"Boolean");
            //AddArgumentToNewMethod(IntAtleast, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntAtleast, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntAtleast);
            
            node_ast *IntAtmost = CreateNewMethod((char *)"ATMOST", (char *)"Boolean");
            //AddArgumentToNewMethod(IntAtmost, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntAtmost, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntAtmost);
            
            node_ast *IntMore = CreateNewMethod((char *)"MORE", (char *)"Boolean");
            //AddArgumentToNewMethod(IntMore, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntMore, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntMore);
            
            node_ast *IntLess = CreateNewMethod((char *)"LESS", (char *)"Boolean");
            //AddArgumentToNewMethod(IntLess, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntLess, (char *)"other", (char *)"Int");
            AddNewMethodToNewClass(IntClass, IntLess);
            
            node_ast *IntEqual = CreateNewMethod((char *)"EQUALS", (char *)"Boolean");
            //AddArgumentToNewMethod(IntEqual, (char *)"this", (char *)"Int");
            AddArgumentToNewMethod(IntEqual, (char *)"other", (char *)"Obj");
            AddNewMethodToNewClass(IntClass, IntEqual);
            
            //Boolean
            node_ast *BooleanClass = CreateNewClass((char *)"Boolean", (char *)"Obj");
            ClassListNode->List.List->push_back(BooleanClass);
            
            node_ast *BooleanString = CreateNewMethod((char *)"STRING", (char *)"String");
            //AddArgumentToNewMethod(BooleanString, (char *)"this", (char *)"Boolean");
            AddNewMethodToNewClass(BooleanClass, BooleanString);
            
            
            //String
            node_ast *StringClass = CreateNewClass((char *)"String", (char *)"Obj");
            ClassListNode->List.List->push_back(StringClass);
            
            node_ast *StringPlus = CreateNewMethod((char *)"PLUS", (char *)"String");
            //AddArgumentToNewMethod(StringPlus, (char *)"this", (char *)"String");
            AddArgumentToNewMethod(StringPlus, (char *)"other", (char *)"String");
            AddNewMethodToNewClass(StringClass, StringPlus);
            
            node_ast *StringEquals = CreateNewMethod((char *)"EQUALS", (char *)"Boolean");
            //AddArgumentToNewMethod(StringEquals, (char *)"this", (char *)"String");
            AddArgumentToNewMethod(StringEquals, (char *)"other", (char *)"Obj");
            AddNewMethodToNewClass(StringClass, StringEquals);
            
            node_ast *StringPrint = CreateNewMethod((char *)"PRINT", (char *)"String");
            //AddArgumentToNewMethod(StringPrint, (char *)"this", (char *)"String");
            AddNewMethodToNewClass(StringClass, StringPrint);
            
            node_ast *StringString = CreateNewMethod((char *)"STRING", (char *)"String");
            //AddArgumentToNewMethod(StringString, (char *)"this", (char *)"String");
            AddNewMethodToNewClass(StringClass, StringString);
            
            
            //Nothing
            node_ast *NothingClass = CreateNewClass((char *)"Nothing", (char *)"Obj");
            ClassListNode->List.List->push_back(NothingClass);
            
            node_ast *NothingEqual = CreateNewMethod((char *)"EQUALS", (char *)"Boolean");
            //AddArgumentToNewMethod(NothingEqual, (char *)"this", (char *)"Obj");
            AddArgumentToNewMethod(NothingEqual, (char *)"other", (char *)"Obj");
            AddNewMethodToNewClass(NothingClass, NothingEqual);
            
            node_ast *NothingPrint = CreateNewMethod((char *)"PRINT", (char *)"Obj");
            //AddArgumentToNewMethod(NothingPrint, (char *)"this", (char *)"Obj");
            AddNewMethodToNewClass(NothingClass, NothingPrint);
            
            node_ast *NothingString = CreateNewMethod((char *)"STRING", (char *)"Nothing");
            //AddArgumentToNewMethod(NothingString, (char *)"this", (char *)"Nothing");
            AddNewMethodToNewClass(NothingClass, NothingString);
            
            
            b32 ConstructorsWellFormed = true;
            WalkAst(RootNode, ClassList, &ConstructorsWellFormed);
            
            if(ConstructorsWellFormed)
            {
                //printf("Constructor calls are well formed.\n");
            }
            else
            {
                printf("Constructor calls are NOT well formed.\n");
            }
            
            b32 IsWellFormed = true;
            list<node_ast *>::const_iterator Iterator;
            for(Iterator = ClassList->begin();
                Iterator != ClassList->end();
                ++Iterator)
            {
                if(*Iterator)
                {
                    ClearVisits(ClassList);
                    
                    node_ast *Node = (*Iterator)->NonTerminal.Children[0];
                    Node->Visited = true;
                    char *BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                    char *ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                    
                    for(;;)
                    {
                        if(FindClassNode(ExtendName, ClassList))
                        {
                            Node = FindClassNode(ExtendName, ClassList);
                            BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                            ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            
                            if(Node->Visited)
                            {
                                //printf("Error A\n");
                                IsWellFormed = false;
                                break;
                            }
                            
                            Node->Visited = true;
                            if(Node->NonTerminal.Children[2]->Ident.Text)
                            {
                                ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            }
                            else
                            {
                                ExtendName = 0;
                            }
                        }
                        else
                        {
                            if(!CompareStrings(ExtendName, (char *)""))
                            {
                                IsWellFormed = false;
                                //printf("Error B %s | %s\n", ExtendName, BaseName);
                            }
                            
                            break;
                        }
                    }
                }
            }
            
            if(!IsWellFormed)
            {
                printf("Class hierarchy is NOT well formed.\n");
            }
            else
            {
                //printf("Class hierarchy is well formed!\n");
                
                VarBubbling = false;
                VarBubblingDone = false;
                
                GlobalContext = RootNode->NonTerminal.Children[1];
                
                BuildContextAST(RootNode, 0, 0, RootNode);
                //printf("check 1\n");
                BuildInstanceVarContext(RootNode);
                //printf("check 2\n");
                CheckClassAndMethodArgNameClash(RootNode, RootNode);
                //printf("check 3\n");
                AddExtensionsAndReturns(RootNode, RootNode);
                //printf("check 4\n");
                CheckAllInstanceVarInit(RootNode);
                //printf("check 5\n");
                //CheckWalk(RootNode);
                CheckArgExtendAndReturnValidity(RootNode, RootNode); 
                //printf("check 6\n");
                CheckReturnFlowExecutionAllMethods(RootNode);
                //printf("check 7\n");
                
                s32 BubbleCount = 0;
                for(BubbleCount = 0;
                    BubbleCount < 32;
                    ++BubbleCount)
                {
                    //printf("bubble %d\n", BubbleCount);
                    VarBubbling = false;
                    BuildVarContext(RootNode, RootNode);
                    //printf("bubble check 1\n");
                    TypeInferAllExpressions(RootNode, RootNode);
                    //printf("bubble check 2\n");
                    
                    if(VarBubbling)
                    {
                        //printf("--------------------\n VARS ARE BUBBLING\n");
                    }
                    else
                    {
                        //printf("--------------------\n vars are done bubbling\n");
                        VarBubblingDone = true;
                    }
                    
                    if(VarBubblingDone)
                    {
                        break;
                    }
                    
                    if(ErrorCount > 0)
                    {
                        break;
                    }
                    
                    printf("next bubble\n");
                }
                
                if(VarBubblingDone)
                {
                    //printf("_done after %d bubbles_\n", BubbleCount);
                    BuildVarContext(RootNode, RootNode);
                    TypeInferAllExpressions(RootNode, RootNode);
                    
                    //printf("_TYPE CHECKING DONE_\n\n\n");
                    
                    if(ErrorCount == 0)
                    {
                        TmpCounter = 0;
                        FILE *CodeFile = fopen("quackcode.c", "w+");
                        fprintf(CodeFile, "#include \"Builtins.c\"\n\n");
                        GenerateHeaderCode(RootNode, CodeFile);
                        GenerateDefCode(RootNode, CodeFile, RootNode);
                        GenerateDefCodeBottom(RootNode, CodeFile);
                        
                        
                        fprintf(CodeFile, "int main() {\n");
                        
                        node_ast *StatementListNode = RootNode->NonTerminal.Children[1];
                        GenerateGlobalStatments(StatementListNode, CodeFile, RootNode);
                        
                        fprintf(CodeFile, "return 0;\n");
                        fprintf(CodeFile, "}\n");
                        fclose(CodeFile);
                    }
                }
                else if(ErrorCount = 0)
                {
                    printf("Made it through max var bubbles and the types still haven't settled!\n");
                }
            }
        }
        else
        {
            printf("Could not read quack file\n");
        }
    }
    else
    {
        printf("No file was passed\n");
        yyparse();
    }
    
    return(0);
}
