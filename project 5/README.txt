All of the components mostly work except for here and there on some 
edge cases. None of the broken things worked properly in previous 
versions of the compiler.

Follow the build instructions to compile quack code!

Build instructions:
bison -d -t quack.y
flex quack.l
g++ quack.c quack.tab.c lex.yy.c -lfl -DYYDEBUG -o quack
quack filename.qk
gcc quackcode.c -o pgrm

Broken stuff:
-Can't put variables as arguments. Code gets generated in such a way
that it will not compile in gcc.

-Strings don't work properly, didn't have enough time to test them
some aspects of the might work but probably not.

-Checking for variable initialization doesn't work

-Somewhere along the way the good/bad type walk test broke. Not sure...

Good stuff:
I think everything else mostly works as expected. Hopefully...