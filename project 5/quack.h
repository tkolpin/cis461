#if !defined(QUACK_H)

#include <list>
#include <stdarg.h>
#include <stdint.h>

using namespace std;

typedef __SIZE_TYPE__ memory_index;

typedef int8_t s08;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef s32 b32;

typedef uint8_t u08;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;

#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

struct node_ast;

enum node_type
{
    NodeType_empty,
    NodeType_class,
    NodeType_ident,
};

struct node_class
{
    node_type Type;
    char *Name;
    char *ExtendName;
    b32 Visited;
};

struct node_ident
{
    node_type Type;
    char *Text;
};

struct node_int_lit
{
    node_type Type;
    char *Text;
};

struct node_string_lit
{
    node_type Type;
    char *Text;
};

enum non_terminal_type
{
    NonTerminalType_plus,
    NonTerminalType_sub,
    NonTerminalType_mult,
    NonTerminalType_div,
    NonTerminalType_unary,
    NonTerminalType_equals,
    NonTerminalType_atmost,
    NonTerminalType_less,
    NonTerminalType_atleast,
    NonTerminalType_more,
    NonTerminalType_and,
    NonTerminalType_or,
    NonTerminalType_not,
    NonTerminalType_constructor,
    NonTerminalType_method,
    NonTerminalType_dot,
    NonTerminalType_program,
    NonTerminalType_class,
    NonTerminalType_statement,
    NonTerminalType_if,
    NonTerminalType_gets,
    NonTerminalType_return,
    NonTerminalType_while,
    NonTerminalType_class_signature,
    NonTerminalType_type,
    NonTerminalType_class_body,
    NonTerminalType_accessor,
    NonTerminalType_rexpression,
};

enum ast_type
{
    AstType_non_terminal,
    AstType_non_terminal_list,
    AstType_ident,
    AstType_int_lit,
    AstType_string_lit,
};

enum list_type
{
    ListType_empty,
    ListType_statement,
    ListType_class,
    ListType_elif,
    ListType_argument,
    ListType_actual_argument,
    ListType_method,
};

struct node_ast;

struct node_non_terminal
{
    non_terminal_type Type;
    
    s32 ChildCount;
    node_ast *Children[32];
};

struct node_list
{
    list<node_ast *> *List;
};

struct quack_arg
{
    char *Name;
    char *Type;
};

struct quack_var
{
    char *Name;
    node_ast *Type;
    b32 IsLocal;
};

struct quack_name
{
    char *Name;
};

struct quack_full_method_list
{
    quack_var List[256];
    s32 Count;
};

struct node_ast
{
    ast_type AstType;
    
    union
    {
        node_non_terminal NonTerminal;
        node_ident Ident;
        node_int_lit IntLit;
        node_string_lit StringLit;
    };
    
    list_type ListType;
    node_list List;
    
    b32 Visited;
    
    char *ClassName;
    char *MethodName;
    
    node_ast *ClassContext;
    node_ast *ClassExtend;
    node_ast *MethodContext;
    node_ast *MethodReturn;
    
    quack_arg ClassArgList[64];
    s32 ClassArgCount;
    quack_arg MethodArgList[64];
    s32 MethodArgCount;
    
    quack_var VarList[256];
    s32 VarCount;
    
    quack_name InstanceVarList[256];
    s32 InstanceVarCount;
    
    quack_name UsedNameList[256];
    s32 UsedNameCount;
    
    node_ast *OverridenFromClass;
    
    node_ast *ExpressionType;
    node_ast *LeftExpressionType;
    
    s32 Tmp;
    s32 TmpEnd;
    s32 LineNumber;
    
    b32 BlankIdent;
    b32 IsInit;
};

node_ast *NewNonTerminalNode(non_terminal_type NonTerminalType, s32 ChildCount, ...);
node_ast *NewIdentNode(char *Name);
node_ast *NewIntLitNode(char *Name);
node_ast *NewStringLitNode(char *Name);
node_ast *NewListNode(list_type ListType);

#define QUACK_H
#endif