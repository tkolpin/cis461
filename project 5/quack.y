%{
#include <cstdio>
#include <iostream>
using namespace std;

#include "quack.h"

extern "C" int yylex();
extern "C" int yyparse();
extern int LineNumber;

extern node_ast *NewNonTerminalNode(non_terminal_type NonTerminalType, s32 ChildCount, ...);
extern node_ast *NewIdentNode(char *Name);
extern node_ast *NewIntLitNode(char *Name);
extern node_ast *NewStringLitNode(char *Name);
extern node_ast *NewListNode(list_type ListType);

extern char Obj[];
extern char Nothing[];

extern node_ast *RootNode;

void yyerror(const char *Text);
%}

%error-verbose

%union
{
	char *Text;
	struct node_ast *NodeAst;
}

%start Program

%type <NodeAst> RExpression LExpression ActualArgumentList Program Class Statement ClassList StatementList StatementBlock ElifList OptionalElse OptionalType OptionalRExpression ActualArgument NonEmptyActualArgumentList ClassSignature ClassBody OptionalExtend ArgumentList NonEmptyArgumentList Argument MethodList Method

%type <Text> IDENT STRING_LIT INT_LIT
%token IDENT

%token CLASS
%token DEF
%token EXTENDS
%token IF
%token ELIF
%token ELSE
%token WHILE
%token RETURN
%token INT_LIT
%token STRING_LIT
%token EQUALS;
%token ATMOST;
%token LESS;
%token ATLEAST;
%token MORE;

%left AND
%left OR
%nonassoc EQUALS ATMOST LESS ATLEAST MORE
%left '+' '-'
%left '*' '/'

%right UNARY NOT

%left '.'

%%

Program:
	ClassList StatementList { RootNode = NewNonTerminalNode(NonTerminalType_program, 2, $1, $2); }
	;

ClassList:
	Class ClassList { $$ = $2; $2->List.List->push_back($1); }
	| /* empty */ { $$ = NewListNode(ListType_class); }
	;

StatementList:
	Statement StatementList { $$ = $2; $2->List.List->push_back($1); }
	| /* empty */ { $$ = NewListNode(ListType_statement); }
	;

Statement:
	IF RExpression StatementBlock ElifList OptionalElse { 
		$$ = NewNonTerminalNode(NonTerminalType_if, 4, $2, $3, $4, $5); }
	| WHILE RExpression StatementBlock { $$ = NewNonTerminalNode(NonTerminalType_while, 2, $2, $3); }
	| RETURN OptionalRExpression ';' { $$ = NewNonTerminalNode(NonTerminalType_return, 1, $2); }
	| LExpression OptionalType '=' RExpression ';' { 
		$$ = NewNonTerminalNode(NonTerminalType_gets, 3, $1, $2, $4); }
	| RExpression ';' { $$ = NewNonTerminalNode(NonTerminalType_rexpression, 1, $1); }
	;

ElifList:
	ELIF RExpression StatementBlock ElifList { $$ = $4; 
		$4->List.List->push_back($2);
		$4->List.List->push_back($3); }
	| /* empty */ { $$ = NewListNode(ListType_elif); }
	;

OptionalElse:
	ELSE StatementBlock{ $$ = $2; }
	| /* empty */ { $$ = NewListNode(ListType_empty); }
	;

OptionalRExpression:
	RExpression { $$ = $1; }
	| /* empty */ { $$ = NewIdentNode(Nothing); }
	;

RExpression:
	STRING_LIT { $$ = NewStringLitNode($1); }
	| INT_LIT { $$ = NewIntLitNode($1); }
	| LExpression { $$ = $1; }
	| RExpression '+' RExpression { $$ = NewNonTerminalNode(NonTerminalType_plus, 2, $1, $3); }
	| RExpression '-' RExpression { $$ = NewNonTerminalNode(NonTerminalType_sub, 2, $1, $3); }
	| RExpression '*' RExpression { $$ = NewNonTerminalNode(NonTerminalType_mult, 2, $1, $3); }
	| RExpression '/' RExpression { $$ = NewNonTerminalNode(NonTerminalType_div, 2, $1, $3); }
	| '-' RExpression %prec UNARY { $$ = NewNonTerminalNode(NonTerminalType_unary, 1, $2); }
	| '(' RExpression ')' { $$ = $2; }
	| RExpression EQUALS RExpression { $$ = NewNonTerminalNode(NonTerminalType_equals, 2, $1, $3); }
	| RExpression ATMOST RExpression { $$ = NewNonTerminalNode(NonTerminalType_atmost, 2, $1, $3); }
	| RExpression LESS RExpression { $$ = NewNonTerminalNode(NonTerminalType_less, 2, $1, $3); }
	| RExpression ATLEAST RExpression { $$ = NewNonTerminalNode(NonTerminalType_atleast, 2, $1, $3); }
	| RExpression MORE RExpression { $$ = NewNonTerminalNode(NonTerminalType_more, 2, $1, $3); }
	| RExpression AND RExpression { $$ = NewNonTerminalNode(NonTerminalType_and, 2, $1, $3); }
	| RExpression OR RExpression { $$ = NewNonTerminalNode(NonTerminalType_or, 2, $1, $3); }
	| NOT RExpression { $$ = NewNonTerminalNode(NonTerminalType_not, 1, $2); }
	| IDENT '(' ActualArgumentList ')' { $$ = NewNonTerminalNode(NonTerminalType_constructor, 2, 
											NewIdentNode($1), $3); }
	| RExpression '.' IDENT '(' ActualArgumentList  ')' { $$ = NewNonTerminalNode(NonTerminalType_accessor, 
															3, $1, NewIdentNode($3), $5); }
	;

LExpression:
	IDENT { $$ = NewIdentNode($1); }
	| RExpression '.' IDENT { $$ = NewNonTerminalNode(NonTerminalType_dot, 2, $1, NewIdentNode($3)); }
	;

ActualArgumentList:
	NonEmptyActualArgumentList { $$ = $1; }
	| /* empty */ { $$ = NewListNode(ListType_actual_argument); }
	;

NonEmptyActualArgumentList:
	ActualArgument { $$ = NewListNode(ListType_actual_argument); $$->List.List->push_back($1); }
	| NonEmptyActualArgumentList ',' ActualArgument { $$ = $1; $1->List.List->push_back($3); }
	;

ActualArgument:
	RExpression { $$ = $1; }
	;

Class:
	ClassSignature ClassBody { $$ = NewNonTerminalNode(NonTerminalType_class, 2, $1, $2); }
	;

ClassSignature:
	CLASS IDENT '(' ArgumentList ')' OptionalExtend { 
		$$ = NewNonTerminalNode(NonTerminalType_class_signature, 3, NewIdentNode($2), $4, $6); }
	;

ArgumentList:
	NonEmptyArgumentList { $$ = $1; }
	| /* empty */ { $$ = NewListNode(ListType_argument); }
	;

NonEmptyArgumentList:
	Argument { $$ = NewListNode(ListType_argument); $$->List.List->push_back($1); }
	| NonEmptyArgumentList ',' Argument { $$ = $1; $1->List.List->push_back($3); }
	;

Argument:
	IDENT ':' IDENT { 
		$$ = NewNonTerminalNode(NonTerminalType_type, 2, NewIdentNode($1), NewIdentNode($3)); }
	;

OptionalExtend:
	EXTENDS IDENT { $$ = NewIdentNode($2); }
	| /* empty */ { $$ = NewIdentNode(Obj); }
	;

ClassBody:
	'{' StatementList MethodList '}' { 
		$$ = NewNonTerminalNode(NonTerminalType_class_body, 2, $2, $3); }
	;

MethodList:
	Method MethodList { $$ = $2; $2->List.List->push_back($1); }
	| /* empty */ { $$ = NewListNode(ListType_method); }
	;

Method:
	DEF IDENT '(' ArgumentList ')' OptionalType StatementBlock { 
		$$ = NewNonTerminalNode(NonTerminalType_method, 4, NewIdentNode($2), $4, $6, $7); }
	;

OptionalType:
	':' IDENT { $$ = NewIdentNode($2); }
	| /* empty */ { $$ = NewIdentNode(Nothing); }
	;

StatementBlock:
	'{' StatementList '}' { $$ = $2; }
	;
