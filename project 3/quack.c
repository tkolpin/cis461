#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#include "quack.h"

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int LineNumber;

void yyerror(const char *Text);

node_ast *RootNode;

char Obj[] = "Obj";

node_ast *
NewListNode(list_type ListType)
{
    node_ast *Result;
    
    node_ast *NodeAst = new node_ast;
    NodeAst->List.List = new list<node_ast *>();
    NodeAst->ListType = ListType;
    NodeAst->AstType = AstType_non_terminal_list;
    
    Result = NodeAst;
#if 0
    printf("Creating List Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal_list, Result->AstType);
    printf("ListType: %d ResultType: %d\n", ListType, Result->ListType);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewNonTerminalNode(non_terminal_type NonTerminalType, s32 ChildCount, ...)
{
    node_ast *Result = new node_ast;
    if(Result)
    {
        Result->AstType = AstType_non_terminal;
        Result->ListType = ListType_empty;
        Result->NonTerminal.Type = NonTerminalType;
        Result->NonTerminal.ChildCount = ChildCount;
        
        list<node_ast *> *List;
        
        va_list VaList;
        va_start(VaList, ChildCount);
        for(s32 Index = 0;
            Index < ChildCount;
            ++Index)
        {
            Result->NonTerminal.Children[Index] = va_arg(VaList, node_ast *);
        }
        
        va_end(VaList);
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
#if 0
    printf("Creating Non Terminal Node\n");
    printf("AstType: %d ResultType: %d\n", AstType_non_terminal, Result->AstType);
    printf("NonTerminalType: %d ResultType: %d\n", NonTerminalType, Result->NonTerminal.Type);
    printf("ListType: %d ResultType: %d\n", ListType_empty, Result->ListType);
    printf("ChildCount: %d ResultCount: %d\n", ChildCount, Result->NonTerminal.ChildCount);
    printf("\n--------------------\n");
#endif
    return(Result);
}

node_ast *
NewIdentNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_ident;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewIntLitNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_int_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

node_ast *
NewStringLitNode(char *Name)
{
    node_ast *Result;
    
    Result = (node_ast *)malloc(sizeof(node_ast));
    
    if(Result)
    {
        Result->AstType = AstType_string_lit;
        Result->Ident.Text = Name;
        Result->ListType = ListType_empty;
    }
    else
    {
        yyerror("System ran out of memory.\n");
    }
    
    return(Result);
}

b32
CompareStrings(char *String1, char *String2)
{
    b32 Result = true;
#if 1
    char *A = String1;
    char *B = String2;
    
    while(*A != '\0' && *B != '\0')
    {
        if(*A++ != *B++)
        {
            Result = false;
            break;
        }
    }
    
    if(*A != *B)
    {
        Result = false;
    }
#endif
    return(Result);
}

node_ast *
FindClassNode(char *Name, list<node_ast *> *ClassList)
{
    node_ast *Result = 0;
    
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *FirstChild = (*Iterator)->NonTerminal.Children[0];
            if(FirstChild)
            {
                char *ClassName = FirstChild->NonTerminal.Children[0]->Ident.Text;
                
                if(CompareStrings(ClassName, Name))
                {
                    Result = FirstChild;
                    break;
                }
            }
        }
    }
    
    return(Result);
}

void
ClearVisits(list<node_ast *> *ClassList)
{
    list<node_ast *>::const_iterator Iterator;
    for(Iterator = ClassList->begin();
        Iterator != ClassList->end();
        ++Iterator)
    {
        if(*Iterator)
        {
            node_ast *Node = (*Iterator)->NonTerminal.Children[0];
            Node->Visited = false;
        }
    }
}

void
WalkAst(node_ast *Node,  list<node_ast *> *ClassList, b32 *ConstructorsWellFormed)
{
    if(Node)
    {
#if 0
        printf("Walking Node\n");
        printf("AstType: %d \n", Node->AstType);
        printf("ListType: %d\n", Node->ListType);
        printf("\n--------------------\n");
#endif
        switch(Node->AstType)
        {
            case AstType_non_terminal:
            {
                if(Node->NonTerminal.Type == NonTerminalType_constructor)
                {
                    node_ast *ConstructorIdentNode = Node->NonTerminal.Children[0];
                    char *ConstructorName = ConstructorIdentNode->Ident.Text;
                    //s32 Bla = 7;
                    //s32 asdf = 8;
                    //s32 boo = 4;
                    if(!FindClassNode(ConstructorName, ClassList))
                    {
                        char Obj[] = "Obj";
                        if(!CompareStrings(ConstructorName, Obj))
                        {
                            *ConstructorsWellFormed = false;
                        }
                    }
                }
                
                for(s32 Index = 0;
                    Index < Node->NonTerminal.ChildCount;
                    ++Index)
                {
                    if(Node->NonTerminal.Children[Index])
                    {
                        WalkAst(Node->NonTerminal.Children[Index], ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
            
            case AstType_non_terminal_list:
            {
                list<node_ast *>::const_iterator Iterator;
                for(Iterator = Node->List.List->begin();
                    Iterator != Node->List.List->end();
                    ++Iterator)
                {
                    if(*Iterator)
                    {
                        WalkAst(*Iterator, ClassList, ConstructorsWellFormed);
                    }
                }
            } break;
        }
    }
}

int
main(int argc, char **argv)
{
#if 0
#ifdef YYDEBUG
    extern int yydebug;
    yydebug = 1;
#endif
#endif
    
    if(argc > 1)
    {
        FILE *File = fopen(argv[1], "r");
        
        if(File)
        {
            printf("Beginning parse of %s\n", argv[1]);
            yyin = File;
            int Result = yyparse();
            
            if(Result ==  0)
            {
                printf("Finished parse with no errors\n");
            }
            else
            {
                printf("Finished parse with some errors\n");
            }
            
            list<node_ast *> *ClassList = RootNode->NonTerminal.Children[0]->List.List;
            
            b32 ConstructorsWellFormed = true;
            //WalkAst2(RootNode, 0, ClassList, &ConstructorsWellFormed);
            WalkAst(RootNode, ClassList, &ConstructorsWellFormed);
            
            if(ConstructorsWellFormed)
            {
                printf("Constructor calls are well formed.\n");
            }
            else
            {
                printf("Constructor calls are NOT well formed.\n");
            }
            
            b32 IsWellFormed = true;
            list<node_ast *>::const_iterator Iterator;
            for(Iterator = ClassList->begin();
                Iterator != ClassList->end();
                ++Iterator)
            {
                if(*Iterator)
                {
                    ClearVisits(ClassList);
                    
                    node_ast *Node = (*Iterator)->NonTerminal.Children[0];
                    Node->Visited = true;
                    char *BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                    char *ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                    
                    for(;;)
                    {
                        if(FindClassNode(ExtendName, ClassList))
                        {
                            Node = FindClassNode(ExtendName, ClassList);
                            BaseName = Node->NonTerminal.Children[0]->Ident.Text;
                            ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            
                            if(Node->Visited)
                            {
                                IsWellFormed = false;
                                break;
                            }
                            
                            Node->Visited = true;
                            if(Node->NonTerminal.Children[2]->Ident.Text)
                            {
                                ExtendName = Node->NonTerminal.Children[2]->Ident.Text;
                            }
                            else
                            {
                                ExtendName = 0;
                            }
                        }
                        else
                        {
                            char Obj[] = "Obj";
                            if(!CompareStrings(ExtendName, Obj))
                            {
                                IsWellFormed = false;
                            }
                            
                            break;
                        }
                    }
                }
            }
            
            if(!IsWellFormed)
            {
                printf("Class hierarchy is NOT well formed.\n");
            }
            else
            {
                printf("Class hierarchy is well formed!\n");
            }
        }
        else
        {
            printf("Could not read quack file\n");
        }
    }
    else
    {
        printf("No file was passed\n");
        yyparse();
    }
    
    return(0);
}
